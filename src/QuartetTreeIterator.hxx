#pragma once
#include <vector>
#include <iostream>
#include "SpeciesTree.hxx"
#include "Classifier.hxx"

using namespace std;

namespace hgt {
  class QuartetTreeIterator {
  public:
    virtual bool finished() { return true; };
    virtual QuartetTree& operator*()=0;
    virtual QuartetTreeIterator& operator++() { return *this; }
    virtual ~QuartetTreeIterator() {}
  };

  // just enumerates all possible quartet trees with weight 1,
  // like a unit-weighted version of quartet_tree_iterator
  class EnumQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    quartet_tree_iterator q;
    QuartetTree qt;
  public:
    EnumQuartetTreeIterator(SpeciesTree &st) :
      q((st.nodecount+1)/2) { qt = QuartetTree(q); qt.weight = 1; }
    EnumQuartetTreeIterator(SpeciesTree &st, int from, int count) :
      q((st.nodecount+1)/2) {
      q.seek(from); q.left = count;
      qt = QuartetTree(q); qt.weight = 1; }
    virtual bool finished() { return !(q.left); }
    virtual QuartetTree& operator*() { return qt; }
    virtual QuartetTreeIterator& operator++()
    { ++q; qt = QuartetTree(q); qt.weight = 1; return *this; }
  };

  class VectorQuartetTreeIterator : public QuartetTreeIterator {
  public:
    vector<QuartetTree> arr;
    unsigned int idx;
    VectorQuartetTreeIterator() { idx = 0; }
    virtual bool finished() { return idx >= arr.size(); };
    virtual QuartetTree& operator*() { return arr[idx]; }
    virtual QuartetTreeIterator& operator++() { ++idx; return *this; }
  };

  void create_map(vector<int> &map, SpeciesTree &from, SpeciesTree &to) {
    map.assign(from.nodecount, -1);
    for (int i=0; i<from.nodecount; i+=2) {
      SpeciesNode *node = to[*(from[i]->get_leaf()->name)];
      if (node) map[i] = node->idx; }
  }

  bool translate(QuartetTree &dest, QuartetTree &src, vector<int> map) {
    int leafs[4];
    src.pairings(leafs[0], leafs[1], leafs[2], leafs[3]);
    for (int i=0; i<4; ++i)
      if (-1 == (leafs[i] = map[leafs[i]])) return false;
    dest.init(leafs, src.weight); return true;
  }

  class TranslatedQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    QuartetTreeIterator &it;
    QuartetTree qt;
    vector<int> mapping;
  public:
    TranslatedQuartetTreeIterator(QuartetTreeIterator &it,
                                  SpeciesTree &from,
                                  SpeciesTree &to) : it(it) {
      create_map(mapping, from, to);
      if (!translate(qt, *it, mapping)) ++(*this);
    }
    virtual bool finished() { return it.finished(); };
    virtual QuartetTree& operator*() { return qt; }
    virtual QuartetTreeIterator& operator++() {
      do ++it; while (!finished() && !translate(qt, *it, mapping));
      return *this; }
  };

  class GenesQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    quartet_tree_iterator q;
    QuartetTree qt;
    int weights[4];
    SpeciesTree &st;
    typedef vector<int> map;
    typedef std::pair<map, SpeciesTree*> gene;
    vector<gene> genes;
    void compute_weights() {
      for (int i=0; i<4; ++i) weights[i] = 0;
      for (vector<gene>::iterator it = genes.begin();
           it != genes.end(); ++it)
        ++weights[pairing_type(*it, q.leafs)];
    }
    int pairing_type(gene &gene, int leafs[4]) {
      SpeciesNode *nodes[4];
      for (int i=0; i<4; ++i)
        if (gene.first[leafs[i]] == -1) return 0; // missing a species
        else nodes[i] = (*(gene.second))[gene.first[leafs[i]]];
      if (match(*(gene.second),
                nodes[0], nodes[1],
                nodes[2], nodes[3])) return 1;
      if (match(*(gene.second),
                nodes[0], nodes[3],
                nodes[2], nodes[1])) return 3;
      // least common case last
      /* if (match(*(gene.second), nodes[0], nodes[2], nodes[1], nodes[3])) */
      return 2;
    }
    // like testing for conflict, but in reverse.
    // mostly copied from Classifier::is_conflicting.
    bool match(SpeciesTree &gt,
               SpeciesNode *a, SpeciesNode *b,
               SpeciesNode *c, SpeciesNode *d) {
      SpeciesNode *AB = gt.get_lca(a, b);
      if (!AB->is_ancestor_of(c) && !AB->is_ancestor_of(d)) return true;
      SpeciesNode *CD = gt.get_lca(c, d);
      if (!CD->is_ancestor_of(a) && !CD->is_ancestor_of(b)) return true;
      return false;
    }
  public:
    GenesQuartetTreeIterator(SpeciesTree &st)
      : q((st.nodecount+1)/2), st(st)  { }
    GenesQuartetTreeIterator(SpeciesTree &st, int from, int count)
      : q((st.nodecount+1)/2), st(st)  { q.seek(from); q.left = count; }
    void add(SpeciesTree *gt) {
      genes.push_back(gene(map(), gt));
      map &map = genes.back().first;
      create_map(map, st, *gt); }
    void ready() { compute_weights(); }
    virtual bool finished() { return !(q.left); };
    virtual QuartetTree& operator*() {
      qt = QuartetTree (q);
      qt.weight = weights[qt.pair];
      return qt; }
    virtual QuartetTreeIterator& operator++()
    { ++q; if (q.pair == 1) compute_weights(); return *this; }
  };

  class ConflictQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    QuartetTreeIterator &it;
    Classifier cf;
  public:
    ConflictQuartetTreeIterator(QuartetTreeIterator &it,
                                SpeciesTree &st) : it(it), cf(st)
    { if (!cf.is_conflicting(*it)) ++(*this); }
    virtual bool finished() { return it.finished(); };
    virtual QuartetTree& operator*() { return *it; }
    virtual QuartetTreeIterator& operator++()
    { do { ++it; }
      while (!finished() && !cf.is_conflicting(*it));
      return *this; }
  };

  class MatchQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    QuartetTreeIterator &it;
    Classifier cf;
  public:
    MatchQuartetTreeIterator(QuartetTreeIterator &it,
                             SpeciesTree &st) : it(it), cf(st)
    { if (cf.is_conflicting(*it)) ++(*this); }
    virtual bool finished() { return it.finished(); };
    virtual QuartetTree& operator*() { return *it; }
    virtual QuartetTreeIterator& operator++()
    { do { ++it; } 
      while (!finished() && cf.is_conflicting(*it));
      return *this; }
  };

  class BootstrapQuartetTreeIterator : public QuartetTreeIterator {
  protected:
    QuartetTreeIterator &it;
    QuartetTree qt;
    int treshold;
  public:
    BootstrapQuartetTreeIterator(QuartetTreeIterator &it,
                                 int treshold) : it(it) {
      this->treshold = treshold;
      qt = QuartetTree(*it);
      qt.weight = 1;
      if ((*it).weight < treshold) ++(*this);
    }
    virtual bool finished() { return it.finished(); };
    virtual QuartetTree& operator*() { return qt; }
    virtual QuartetTreeIterator& operator++()
    { do { ++it; } 
      while (!finished() && ((*it).weight < treshold));
      qt = QuartetTree(*it);
      qt.weight = 1;
      return *this; }
  };
}
