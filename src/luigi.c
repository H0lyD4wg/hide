#include "lua.h"
#include "lauxlib.h"
#include "luigi.h"

void luigi_getmetatable(lua_State *L,
                        const char *lib,
                        const char *name) {
  lua_pushstring(L, lib);
  lua_rawget(L, LUA_REGISTRYINDEX);
  lua_pushstring(L, name);
  lua_rawget(L, -2);
  lua_remove(L, -2);
}

void luigi_create_metatable(lua_State *L,
                            const char *tname,
                            const luaL_Reg *reg) {
  lua_pushstring(L, tname);
  lua_newtable(L);
  lua_pushliteral(L, "__index");
  lua_pushvalue(L, -2);
  lua_rawset(L, -3);
  luaL_register(L, NULL, reg);
  lua_rawset(L, -3);
}

void *luigi_checkudata(lua_State *L, int ud,
                       const char *lib, const char *tname) {
  void *p = lua_touserdata(L, ud);
  if (p != NULL) {  // value is a userdata?
    if (lua_getmetatable(L, ud)) {  // does it have a metatable?
      luigi_getmetatable(L, lib, tname);
      if (lua_rawequal(L, -1, -2)) {  // does it have the correct mt?
        lua_pop(L, 2);  // remove both metatables
        return p; }}}
  luaL_typerror(L, ud, tname);  // else error
  return NULL;  // to avoid warnings
}

void luigi_push_udata(lua_State *L, void *data,
                      const char *lib, const char *tname) {
  void **ptr = (void**) lua_newuserdata(L, sizeof(void*));
  (*ptr) = data;
  luigi_getmetatable(L, lib, tname);
  lua_setmetatable(L, -2);
}

int luigi_argcheck(lua_State *L, int n) {
  if (lua_gettop(L) != n)
    return luaL_error(L, "wrong number of arguments. "
                      "expected %d, got %d.", n, lua_gettop(L));
  else return 0;
}

int luigi_argcheck2(lua_State *L, int n1, int n2) {
  if ((lua_gettop(L) != n1) && (lua_gettop(L) != n2))
    return luaL_error(L, "wrong number of arguments. "
                      "expected %d or %d, got %d.", n1, n2, lua_gettop(L));
  else return lua_gettop(L);
}
