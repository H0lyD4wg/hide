
#pragma once
#include "DecoratedSpeciesTree.hxx"
#include "Classifier.hxx"

namespace hgt {
  typedef struct triple {
  public:
    int weight;
    int root_idx;
    bool is_scp;
    triple(int weight, int root_idx, bool is_scp) :
      weight(weight), root_idx(root_idx), is_scp(is_scp) {}
  } triple;

  class AsymmetricDecoratedTree :
    public DecoratedSpeciesTree<triple> {
  
  public:
    AsymmetricDecoratedTree(SpeciesTree &st) :
      DecoratedSpeciesTree<triple>(st) { }
    void hang(int weight, const spscp_pair p);
    void hang(int weight, const spscp_pair ps[4])
    { for (int i=0; i<4; ++i) hang(weight, ps[i]); }
  
  protected:
    virtual void augment(int vidx, int *val);
    virtual void compute_vals(int uidx, vector<int> &counter, int *val);
  };
}