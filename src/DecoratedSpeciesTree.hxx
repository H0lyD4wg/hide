
#pragma once
#include <deque>
#include <vector>
#include "SpeciesTree.hxx"
#include "QuartetTree.hxx"

namespace hgt {
  using std::vector;
  using std::deque;

  static bool is_meaningful(SpeciesNode *u, SpeciesNode *v) {
    return !(u->is_ancestor_of(v) || v->is_ancestor_of(u) ||
             (u->parent == v->parent));
  }

  template<typename decoration_t>
  class DecoratedSpeciesTree {
  public:
    DecoratedSpeciesTree(SpeciesTree &st) : st(st) {
      for (int i=0; i<st.nodecount; ++i)
        path.push_back(deque<decoration_t>());
    }
  
    void compute_scores(float **scores) { csvisit(st.root_idx, scores); }
  
  protected:
    SpeciesTree &st;
    vector<deque<decoration_t> > path;
    virtual void augment(int vidx, int *val)=0;
    virtual void compute_vals(int uidx, vector<int> &counter, int *val)=0;
  
    void csvisit(int vidx, float **scores) {
      SpeciesNode *v = st[vidx];
      SpeciesInternalNode *vint = v->get_internal();
      SpeciesLeafNode *vleaf = v->get_leaf();
  
      if (vint) {
        csvisit(vint->left->idx, scores);
        csvisit(vint->right->idx, scores);
      }
  
      int *val = new int[st.nodecount];
      augment(vidx, val);
  
      for (int uidx=0; uidx < st.nodecount; ++uidx) {
        SpeciesNode *u = st[uidx];
        if (!is_meaningful(u, v)) {
          scores[uidx][vidx] = -1;
          continue; }
        if (vleaf) scores[uidx][vidx] = val[uidx];
        if (vint) scores[uidx][vidx] =
                    scores[uidx][vint->left->idx] +
                    scores[uidx][vint->right->idx] -
                    val[uidx];
      }
  
      delete[] val;
    }
  };

}
