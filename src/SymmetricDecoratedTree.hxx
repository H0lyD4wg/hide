
#pragma once
#include "DecoratedSpeciesTree.hxx"
#include "Classifier.hxx"

namespace hgt {
  typedef struct symmetric_triple {
  public:
    int weight;
    int top_idx;
    int bottom_idx;
    symmetric_triple(int weight, int top_idx, int bottom_idx) :
      weight(weight), top_idx(top_idx), bottom_idx(bottom_idx) {}
  } symmetric_triple;

  class SymmetricDecoratedTree :
    public DecoratedSpeciesTree<symmetric_triple> {
  
  public:
    SymmetricDecoratedTree(SpeciesTree &st)  :
      DecoratedSpeciesTree<symmetric_triple>(st) { }
    void hang(int weight, const pp_pair p);
    void hang(int weight, const pp_pair ps[2])
    { for (int i=0; i<2; ++i) hang(weight, ps[i]); }
  
  protected:
    virtual void augment(int vidx, int *val);
    virtual void compute_vals(int uidx, vector<int> &counter, int *val);
  };
}