#pragma once
#include <string>
#include <vector>
#include "lua.hpp"
#include "SpeciesTree.hxx"
#include "QuartetTree.hxx"

namespace hgt {
  bool QuartetTreesFromTable(SpeciesTree &st,
                             std::vector<QuartetTree> &qts,
                             lua_State *L);
  bool SpeciesTreeFromTable(SpeciesTree &st, lua_State *L);
}
