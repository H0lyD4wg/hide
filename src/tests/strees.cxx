SUITE(species_trees) {

TEST(species_tree_nodecount) {
  SpeciesTree st;
  Pokemon(st);
  CHECK_EQUAL(2*9-1, st.nodecount);
  // make sure that the last node acutally exists
  CHECK(st[2*9-2]);
}

TEST(species_node_indexes) {
  SpeciesTree st;
  Pokemon(st);
  for (int i=0; i<st.nodecount; ++i)
    CHECK_EQUAL(i, st[i]->idx);
}

TEST(species_tree_squirtle) {
  SpeciesTree st;
  SpeciesNode *sqidx, *sqname;
  Pokemon(st);

  CHECK(sqidx = st[6]); // 3rd leaf == 6th node
  CHECK(sqname = st[string("Squirtle")]);
  CHECK_EQUAL(sqidx, sqname);
  CHECK(sqidx->get_leaf());
  CHECK_EQUAL("Squirtle", sqidx->get_leaf()->name->data());
  CHECK_EQUAL(6, sqname->idx);
}

TEST(exactly_one_nonconflicting_pairing_type) {
  SpeciesTree st; Pokemon(st); Classifier cf(st);
  quartet_tree_iterator it(9);
  for (int i=0; i<126; ++i) {
    int matches = 0 ;
    for (int j=1; j<=3; ++j, ++it)
      if (!cf.is_conflicting(it)) ++matches;
    CHECK_EQUAL(1, matches); }
}

TEST(pairing_type_2_always_conflicts) {
  SpeciesTree st; Pokemon(st); Classifier cf(st);
  quartet_tree_iterator it(9);
  for (int i=0; i<126; ++i)
    for (int j=1; j<=3; ++j, ++it)
      CHECK((j!=2)||cf.is_conflicting(it));
}

}
