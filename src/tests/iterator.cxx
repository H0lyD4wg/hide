SUITE(quartet_iterators) {

TEST(enumerate_quartets_correct_indexes) {
  int idx[4];
  quartet_iterator it(9);
  for (int i=0; i<126; ++i, ++it) {
    kth_quartet(i, 9, idx);
    CHECK_ARRAY_EQUAL(idx, it.idx, 4); } }

TEST(quartet_seek_k) {
  quartet_iterator it(9);
  quartet_iterator ti(9);
  for (int i=0; i<25; ++i, ++it);
  ti.seek(25);
  for (int i=0; i<50; ++i, ++it, ++ti) {
    CHECK_EQUAL(it.where, ti.where);
    CHECK_ARRAY_EQUAL(it.idx, ti.idx, 4); } }

TEST(quartet_seek_xs) {
  int idx[4] = {0, 1, 4, 7};
  quartet_iterator it(9);
  quartet_iterator ti(9);
  for (int i=0; i<13; ++i, ++it);
  ti.seek(idx);  
  for (int i=0; i<50; ++i, ++it, ++ti) {
    CHECK_EQUAL(it.where, ti.where);
    CHECK_ARRAY_EQUAL(it.idx, ti.idx, 4); } }

TEST(quartet_tree_seek_k) {
  quartet_tree_iterator it(9);
  quartet_tree_iterator ti(9);
  for (int i=0; i<20; ++i, ++it);
  ti.seek(20);
  for (int i=0; i<50; ++i, ++it, ++ti) {
    CHECK_EQUAL(it.pair, ti.pair);
    CHECK_ARRAY_EQUAL(it.leafs, ti.leafs, 4); } }

TEST(quartet_tree_seek_xs) {
  int leafs[4] = {0, 2, 8, 14};
  quartet_tree_iterator it(9);
  quartet_tree_iterator ti(9);
  for (int i=0; i<(39+1); ++i, ++it);
  ti.seek(leafs, 2);
  for (int i=0; i<50; ++i, ++it, ++ti) {
    CHECK_EQUAL(it.pair, ti.pair);
    CHECK_ARRAY_EQUAL(it.leafs, ti.leafs, 4); } }

TEST(limited_iterator) {
  SpeciesTree st; Pokemon(st);
  EnumQuartetTreeIterator it(st);
  EnumQuartetTreeIterator ti(st, 70, 50);
  for (int i=0; i<70; ++i, ++it);
  int i = 0;
  for (i=0; !(ti.finished()); ++it, ++ti, ++i) {
    CHECK_EQUAL((*it).pair, (*ti).pair);
    CHECK_ARRAY_EQUAL((*it).leafs, (*ti).leafs, 4);
  }
  CHECK_EQUAL(50, i); }

TEST(limited_iterator_bonk) {
  SpeciesTree st; Pokemon(st);
  EnumQuartetTreeIterator it(st, 126*3-20, 50);
  int i = 0;
  for (i=0; !(it.finished()); ++it, ++i);
  CHECK_EQUAL(20, i); }
}
