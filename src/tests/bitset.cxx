std::pair<int, int> riap(std::pair<int, int> pair) {
  return std::pair<int, int>(pair.second, pair.first);
}

typedef bitset<126*3> qset;

class BitsetComputer {
public:
  SpeciesTree st;

  BitsetComputer() {
    Pokemon(st);
  }

  qset **alloc_setmatrix() {
    qset *data = new qset[17*17];
    qset **rows = new qset*[17];
    for (int i=0; i<17; ++i)
      rows[i] = data + 17*i;
    return rows;
  }

  void delete_setmatrix(qset **rows) {
    delete[] rows[0];
    delete[] rows;
  }

  void zero(qset **rows) {
    qset *it = rows[0];
    for (int i=0; i<(17*17); ++i, ++it) *it = qset(0);
  }

  void fold_dsets(qset **csets, qset **dsets) {
    for (int i=0; i<st.nodecount; ++i)
      for (int j=i+1; j<st.nodecount; ++j) {
        if (!is_meaningful(st[i], st[j])) continue;
        csets[i][j] = dsets[i][j] | dsets[j][i];
      }
  }

  void compute_qsets(qset **ssets, qset **dsets) {
    vector<std::pair<int, int> > edges; // empty
    compute_qsets(ssets, dsets, edges);
  }

  void compute_qsets(qset **ssets, qset **dsets,
                     vector<std::pair<int,int> > &edges) {
    float **sscores = alloc_matrix(st.nodecount);
    float **dscores = alloc_matrix(st.nodecount);

    Classifier cf(st);
    EdgeFilter ef(st, edges);

    zero(ssets);
    zero(dsets);

    quartet_tree_iterator it(9);
    for (int chunk=0; chunk<126; chunk+=5) {
      SymmetricDecoratedTree sdt(st);
      AsymmetricDecoratedTree adt(st);

      for (int i=0; (i<5*3) && (it.left); ++i, ++it) {
        if (!cf.is_conflicting(it)) continue;

        spscp_pair ddecs[4];
        pp_pair sdecs[2];
        cf.gen_decs(it, ddecs, sdecs);
        if (!ef.should_ignore(ddecs)) {
          adt.hang(1<<i, ddecs);
          sdt.hang(1<<i, sdecs); }
      }

      int shift = chunk*3;
      qset reg;

      sdt.compute_scores(sscores);
      for (int i=0; i<st.nodecount; ++i)
        for (int j=i+1; j<st.nodecount; ++j) {
          if (!is_meaningful(st[i], st[j])) continue;
          reg = qset((long)sscores[i][j]);
          reg <<= shift;
          ssets[i][j] |= reg;
        }

      adt.compute_scores(dscores);
      for (int i=0; i<st.nodecount; ++i)
        for (int j=0; j<st.nodecount; ++j) {
          if (!is_meaningful(st[i], st[j])) continue;
          reg = qset((long)dscores[i][j]);
          reg <<= shift;
          dsets[i][j] |= reg;
        }
    }

    delete_matrix(sscores);
    delete_matrix(dscores);
  }

  void find_top_edge(qset **fsets, int &x, int &y) {
    int top_score = -1;
    for (int i=0; i<st.nodecount; ++i)
      for (int j=i+1; j<st.nodecount; ++j) {
        if (!is_meaningful(st[i], st[j])) continue;
        if ((int)fsets[i][j].count() > top_score) {
          x = i; y = j;
          top_score = fsets[i][j].count();
        }
      }
  }

  void compute_multiple_hgts(int count,
                             vector<qset**> &fsets_arr,
                             vector<std::pair<int, int> > &edges) {
    vector<std::pair<int, int> > double_edges;
    for (vector<std::pair<int, int> >::iterator it = edges.begin();
         it != edges.end(); it++) {
      double_edges.push_back(*it);
      double_edges.push_back(riap(*it));
    }

    qset **ssets = alloc_setmatrix();
    qset **dsets = alloc_setmatrix();

    qset **fsets;

    for (int i=0; i<count; ++i) {
      compute_qsets(ssets, dsets, double_edges);
      fsets = alloc_setmatrix();
      fold_dsets(fsets, dsets);
      fsets_arr.push_back(fsets);

      int x=-1, y=-1;
      find_top_edge(fsets, x, y);
      std::pair<int, int> edge(x, y);
      edges.push_back(edge);
      double_edges.push_back(edge);
      double_edges.push_back(riap(edge));
    }

    delete_setmatrix(ssets);
    delete_setmatrix(dsets);
  }
};

TEST_FIXTURE(BitsetComputer, symmetric_score_is_intersection) {
  qset **ssets = alloc_setmatrix();
  qset **dsets = alloc_setmatrix();
  compute_qsets(ssets, dsets);
  for (int i=0; i<st.nodecount; ++i)
    for (int j=i+1; j<st.nodecount; ++j) {
      if (!is_meaningful(st[i], st[j])) continue;
      CHECK_EQUAL(ssets[i][j], dsets[i][j] & dsets[j][i]);
    }
  delete_setmatrix(ssets);
  delete_setmatrix(dsets);
}

qset ignored_quartets(SpeciesTree &st, vector<std::pair<int, int> > &edges) {
  Classifier cf(st); EdgeFilter ef(st, edges);
  qset mask = qset(0); int counter=0;
  quartet_tree_iterator it(9);
  for (; it.left; ++it, ++counter) {
    if (!cf.is_conflicting(it)) continue;

    spscp_pair ddecs[4];
    pp_pair sdecs[2];
    cf.gen_decs(it, ddecs, sdecs);
    mask.set(counter, ef.should_ignore(ddecs));
  }

  return mask;
}

TEST_FIXTURE(BitsetComputer, edge_filter_ignored_quartets) {
  vector<qset**> fsets_arr;
  vector<std::pair<int, int> > top_edges;
  compute_multiple_hgts(3, fsets_arr, top_edges);

  vector<std::pair<int, int> > ignored_edges;

  ignored_edges.push_back(top_edges[0]);
  ignored_edges.push_back(riap(top_edges[0]));
  qset top0 = fsets_arr[0]
    [top_edges[0].first]
    [top_edges[0].second];
  CHECK_EQUAL(top0, ignored_quartets(st, ignored_edges));

  ignored_edges.push_back(top_edges[1]);
  ignored_edges.push_back(riap(top_edges[1]));
  qset top1 = fsets_arr[0]
    [top_edges[1].first]
    [top_edges[1].second];
  CHECK_EQUAL(top0 | top1, ignored_quartets(st, ignored_edges));

  for (vector<qset**>::iterator it = fsets_arr.begin();
       it != fsets_arr.end(); it++)
    delete_setmatrix(*it);
}

TEST_FIXTURE(BitsetComputer, edge_filtered_score) {
  vector<qset**> fsets_arr;
  vector<std::pair<int, int> > top_edges;
  compute_multiple_hgts(3, fsets_arr, top_edges);

  qset top0 = fsets_arr[0]
    [top_edges[0].first]
    [top_edges[0].second];

  for (int i=0; i<st.nodecount; ++i)
    for (int j=i+1; j<st.nodecount; ++j) {
      if (!is_meaningful(st[i], st[j])) continue;
      CHECK_EQUAL(fsets_arr[0][i][j] & ~top0, fsets_arr[1][i][j]);
    }

  qset top1 = fsets_arr[0]
    [top_edges[1].first]
    [top_edges[1].second];

  for (int i=0; i<st.nodecount; ++i)
    for (int j=i+1; j<st.nodecount; ++j) {
      if (!is_meaningful(st[i], st[j])) continue;
      CHECK_EQUAL(fsets_arr[0][i][j] & ~top0 & ~top1, fsets_arr[2][i][j]);
    }

  for (vector<qset**>::iterator it = fsets_arr.begin();
       it != fsets_arr.end(); it++)
    delete_setmatrix(*it);
}
