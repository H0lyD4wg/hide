SUITE(quartet_utils) {

TEST(nine_over_four_equals_126) {
  CHECK_EQUAL(choose4(9), 126);
}

TEST(kth_quartet_first) {
  int idx[4];
  int expected[4] = {0, 1, 2, 3};
  CHECK(kth_quartet(0, 9, idx));
  CHECK_ARRAY_EQUAL(expected, idx, 4);
}

TEST(find_quartet_first) {
  int idx[4] = {0, 1, 2, 3};
  CHECK_EQUAL(0, find_quartet(9, idx));
}

TEST(kth_quartet_last) {
  int idx[4];
  int expected[4] = {5, 6, 7, 8};
  CHECK(kth_quartet(125, 9, idx));
  CHECK_ARRAY_EQUAL(expected, idx, 4);
}

TEST(find_quartet_last) {
  int idx[4] = {5, 6, 7, 8};
  CHECK_EQUAL(125, find_quartet(9, idx));
}

TEST(kth_quartet_after_last) {
  int idx[4];
  CHECK(!kth_quartet(126, 9, idx));
}

TEST(kth_quartet_13_0147) {
  int idx[4];
  int expected[4] = {0, 1, 4, 7};
  CHECK(kth_quartet(13, 9, idx));
  CHECK_ARRAY_EQUAL(expected, idx, 4);
}

TEST(find_quartet_0147_13) {
  int idx[4] = {0, 1, 4, 7};
  CHECK_EQUAL(13, find_quartet(9, idx));
}

TEST(find_kth_inverse) {
  int idx[4];
  int sample[6] = {100, 81, 27, 42, 11, 23};
  for (int i=0; i<6; ++i) {
    kth_quartet(sample[i], 9, idx);
    CHECK_EQUAL(sample[i], find_quartet(9, idx));
  }
}

TEST(kth_find_inverse) {
  int idx[4];
  int sample[6][4] = { {3, 4, 7, 8},
                       {1, 5, 6, 7},
                       {2, 3, 4, 5},
                       {2, 3, 4, 7},
                       {0, 5, 6, 7},
                       {0, 2, 4, 6} };
  for (int i=0; i<6; ++i) {
    kth_quartet(find_quartet(9, sample[i]), 9, idx);
    CHECK_ARRAY_EQUAL(sample[i], idx, 4);
  }
}

}
