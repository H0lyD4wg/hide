
#include "AsymmetricDecoratedTree.hxx"

namespace hgt {
  void AsymmetricDecoratedTree::hang(int weight, spscp_pair p) {
    path[p.bottom].push_back(triple(weight, p.root, p.is_scp));
    path[st[p.top]->parent->idx]
      .push_back(triple(weight, p.root, p.is_scp));
  }

  void AsymmetricDecoratedTree::augment(int vidx, int *val) {
    vector<int> counter;
    counter.assign(st.nodecount, 0);
  
    deque<triple>::iterator it;
    for (it = path[vidx].begin(); it != path[vidx].end(); it++) {
      if (!it->is_scp) { // SP
        counter[it->root_idx] += it->weight;
      } else { // SCP
        counter[st.root_idx] += it->weight;
        SpeciesInternalNode *node;
        if ((node = st[it->root_idx]->get_internal())) {
          counter[node->left->idx] -= it->weight;
          counter[node->right->idx] -= it->weight;
        }
      }
    }
  
    compute_vals(st.root_idx, counter, val);
  }
  
  // helper method used by ``augment''
  void AsymmetricDecoratedTree::compute_vals(int uidx,
					     vector<int> &counter,
					     int *val) {
    SpeciesNode *p = st[uidx]->parent;
    SpeciesInternalNode *u;
    val[uidx] = counter[uidx] + (p ? val[p->idx] : 0);
    if ((u = st[uidx]->get_internal())) {
      compute_vals(u->left->idx, counter, val);
      compute_vals(u->right->idx, counter, val);
    }
  }
}
