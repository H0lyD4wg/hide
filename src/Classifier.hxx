#pragma once
#include "SpeciesTree.hxx"
#include "QuartetTree.hxx"
#include "quartets.hxx"

namespace hgt {
  typedef struct spscp_pair {
    bool is_scp;
    int root; // root of the subtree
    int top, bottom; // endpoints of the path
  } spscp_pair;

  typedef struct pp_pair {
    int ltop, lbottom; // endpoints of the left path
    int rtop, rbottom; // endpoints of the right path
  } pp_pair;

  enum qtree_topology {TYPE_1, TYPE_2LL, TYPE_2LR, TYPE_2RL, TYPE_2RR};

  static inline int pair(qtree_topology topo) {
    switch (topo) {
    case TYPE_1:
    case TYPE_2LL:
    case TYPE_2RR:
      return 1;
    case TYPE_2LR:
    case TYPE_2RL:
      return 3;
    default:
      // pacify the overly-worried compiler
      return -1;
    }
  }

  class Classifier {
  private:
    SpeciesTree &species_tree;
  public:
    Classifier(SpeciesTree &species_tree) : species_tree(species_tree) { }
  
    bool is_conflicting(QuartetTree qt);
    void gen_decs(QuartetTree qt, spscp_pair *ddecs, pp_pair *sdecs);
    qtree_topology classify(QuartetTree qt);
  
  private:
    void get_internal_nodes(QuartetTree qt, SpeciesNode *EFG[3]);
  
    void make_tree(QuartetTree qt,
                   bool &is_scp, int &root,
                   SpeciesNode *X, SpeciesNode *x);
  
    void make_path(QuartetTree qt,
                   int &top, int &bottom,
                   SpeciesNode *X, SpeciesNode *x);
  
    void assign_pairings(QuartetTree &qt,
                         SpeciesNode *&a,
                         SpeciesNode *&b,
                         SpeciesNode *&c,
                         SpeciesNode *&d);
  };
}
