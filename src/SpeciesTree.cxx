#include <vector>
#include <iostream>
#include "SpeciesTree.hxx"

namespace hgt {
  SpeciesTree::~SpeciesTree() {
    if (rmqinfo) rm_free(rmqinfo);
    rmqinfo = NULL;
  
    for (std::vector<SpeciesNode*>::iterator it = nodes.begin();
         it < nodes.end(); it++) {
      if (NULL != (*it)->get_leaf())
        delete (*it)->get_leaf();
      else
        delete (*it)->get_internal();
    }
  }
  
  SpeciesNode* SpeciesTree::operator[](const std::string &speciesname) {
    if (species.find(&speciesname) != species.end())
      return species[&speciesname];
    else return NULL;
  }
  
  SpeciesNode* SpeciesTree::operator[](int idx) {
    return nodes.at(idx);
  }
  
  SpeciesNode* SpeciesTree::get_lca(SpeciesNode *u, SpeciesNode *v) {
    return nodes[rm_query(rmqinfo, u->idx, v->idx)];
  }
}