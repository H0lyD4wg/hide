#include <lua.h>
#include <lauxlib.h>

void luigi_getmetatable(lua_State *L, const char *lib, const char *name);
void luigi_create_metatable(lua_State *L,
                            const char *tname,
                            const luaL_Reg *reg);
void *luigi_checkudata(lua_State *L, int ud,
                       const char *lib, const char *tname);
void luigi_push_udata(lua_State *L, void *data,
                      const char *lib, const char *tname);
int luigi_argcheck(lua_State *L, int n);
int luigi_argcheck2(lua_State *L, int n1, int n2);
