#pragma once
#include "quartets.hxx"

namespace hgt {
  class QuartetTree {
  public:
    int leafs[4], pair, weight;
    QuartetTree();
    int& operator[](int i) { return leafs[i]; }
    int pair_of(int idx);
    void pairings(int &a, int &b, int &c, int &d);
    bool init(const int leafs[], int weight);

    // transitional convenience ctor
    QuartetTree(quartet_tree_iterator &it) {
      for (int i=0; i<4; ++i) leafs[i]=it[i];
      pair = it.pair; }
  };
}
