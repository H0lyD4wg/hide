#include "ast.hxx"
#include "lua.hpp"

extern "C" { int luaopen_lpeg (lua_State *L); }

namespace hgt {
  SpeciesNode* visit_node(lua_State *L, SpeciesTree &st, int depth) {
    if (lua_isstring(L, -1)) {
      SpeciesLeafNode *leaf =
        new SpeciesLeafNode(st.nodecount++,
                            std::string(lua_tostring(L, -1)));
      st.nodes.push_back((SpeciesNode*)leaf);
      st.depths.push_back(depth);
      if (st.species.find(leaf->name) != st.species.end())
        /* TODO: how to deal with duplicate names? */;
      st.species[leaf->name] = leaf;
      return leaf;
    }

    else if (lua_istable(L, -1) && (2 == lua_objlen(L, -1))) {
      lua_checkstack(L, 1); // make sure we don't overflow

      lua_rawgeti(L, -1, 1);
      SpeciesNode *left = visit_node(L, st, depth+1);
      lua_remove(L, -1);

      SpeciesInternalNode *internal = new SpeciesInternalNode(st.nodecount++);
      st.nodes.push_back((SpeciesNode*)internal);
      st.depths.push_back(depth);

      lua_rawgeti(L, -1, 2);
      SpeciesNode *right = visit_node(L, st, depth+1);
      lua_remove(L, -1);

      // child pointers
      internal->left = left;
      internal->right = right;

      // parent pointers
      left->parent = internal;
      right->parent = internal;

      // range endpoints
      internal->range_left = left->range_left;
      internal->range_right = right->range_right;

      return internal;
    }

    else return NULL;
  }

  bool SpeciesTreeFromTable(SpeciesTree &st, lua_State *L) {
    st.nodecount = 0;
    st.root_idx = -1;
    SpeciesNode *root = visit_node(L, st, 0);
    st.root_idx = root->idx;
    root->parent = NULL;
    st.rmqinfo = rm_query_preprocess(&st.depths[0], st.depths.size());
    lua_remove(L, -1);
    return true;
  }

  bool QuartetTreesFromTable(SpeciesTree &st,
                             std::vector<QuartetTree> &qts,
                             lua_State *L) {
    if (!lua_istable(L, -1)) return false;
    int length = lua_objlen(L, -1);
    int leafs[4];
    QuartetTree qt;

    for (int i=1; i<=length; ++i) {
      lua_rawgeti(L, -1, i);
      bool flag = false; // for unrecognized species
      for (int k=0; k<4; ++k) {
        lua_rawgeti(L, -1, k+1);
        const char *name = lua_tostring(L, -1);
        SpeciesNode *node = st[std::string(name)];
        if (node) leafs[k] = node->idx;
        else flag = true;
        lua_pop(L, 1);
      }
      lua_getfield(L, -1, "weight");
      int weight = lua_tointeger(L, -1);
      if (!flag && qt.init(leafs, weight)) qts.push_back(qt);
      lua_pop(L, 2);
    }

    lua_remove(L, -1);
    return true;
  }
}
