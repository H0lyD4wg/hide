#pragma once
#include "SpeciesTree.hxx"

namespace hgt {
  bool kth_quartet(int k, int n, int idx[4]);
  
  int find_quartet(int n, const int idx[4]);

  #ifdef __GNUC__
  // supress warnings about functions being unused.
  static int choose4(int n) __attribute__ ((__unused__));
  static int choose3(int n) __attribute__ ((__unused__));
  static int choose2(int n) __attribute__ ((__unused__));
  static int choose1(int n) __attribute__ ((__unused__));
  #endif
  
  static int choose4(int n) { return (n*(n-1)*(n-2)*(n-3))/24; }
  static int choose3(int n) { return (n*(n-1)*(n-2))/6; }
  static int choose2(int n) { return (n*(n-1))/2; }
  static int choose1(int n) { return n; }

  class quartet_iterator {
  public:
    int n;
    int idx[4];
    int where;
    quartet_iterator(int n)
    { this->n = n; where = 0; for (int i=0; i<4; ++i) idx[i]=i; }
    int operator[](const int which) { return idx[which]; }
    quartet_iterator& operator++() {
      if (idx[0] == n-4) { where = -1; return *this; }
      ++where;
      int k;
      for(k=3; idx[k] >= n-4+k; --k);
      ++idx[k++];
      for (; k<4; ++k) idx[k] = idx[k-1]+1;
      return *this; }

    void seek(int k) { where = k; kth_quartet(k, n, idx); }
    void seek(const int xs[4]) {
      for (int i=0; i<4; ++i) idx[i] = xs[i];
      where = find_quartet(n, idx); }
  };

  class quartet_tree_iterator {
  public:
    quartet_iterator q;
    int leafs[4], pair;
    int left; // how many qtrees to go?
    quartet_tree_iterator(const int n) : q(n) {
      left = choose4(n)*3;
      pair = 1;
      for (int i=0; i<4; ++i) leafs[i] = i*2; }
    int operator[](const int which) { return leafs[which]; }
    quartet_tree_iterator& operator++() {
      --left;
      if (pair == 3) {
        ++q; pair = 1; if (q.where == -1) { left = 0; return *this; }
        for (int i=0; i<4; ++i) leafs[i] = q[i]*2; }
      else ++pair;
      return *this;
    }

    void seek(int k) {
      q.seek(k/3); pair = 1+(k%3);
      for (int i=0; i<4; ++i) leafs[i] = q[i]*2; }
    void seek(const int xs[4], int pair) {
      int idx[4];
      for (int i=0; i<4; ++i) idx[i] = xs[i]/2;
      seek(find_quartet(q.n, idx)*3 + pair - 1); }
  };
}
