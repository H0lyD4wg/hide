#include "quartets.hxx"

namespace hgt {
  bool kth_quartet(int k, int n, int idx[4]) {
    if (k >= choose4(n)) return false;
    for (--n, idx[0]=0; k >= choose3(n);
         k -= choose3(n), --n, ++idx[0]);
    for (--n, idx[1]=idx[0]+1; k >= choose2(n);
         k -= choose2(n), --n, ++idx[1]);
    for (--n, idx[2]=idx[1]+1; k >= choose1(n);
         k -= choose1(n), --n, ++idx[2]);
    idx[3] = idx[2]+1+k;
    return true;
  }
  
  int find_quartet(int n, const int idx[4]) {
    int k=0, delta;
    for (--n, delta=0; delta < idx[0];
         k += choose3(n), --n, ++delta);
    for (--n, delta=idx[0]+1; delta < idx[1];
         k += choose2(n), --n, ++delta);
    for (--n, delta=idx[1]+1; delta < idx[2];
         k += choose1(n), --n, ++delta);
    k += idx[3] - (idx[2] + 1);
    return k;
  }
}
