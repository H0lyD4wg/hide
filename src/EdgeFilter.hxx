#include "SpeciesTree.hxx"
#include "Classifier.hxx"

namespace hgt {
  class EdgeFilter {
  public:
    SpeciesTree &st;
    vector<std::pair<int,int> > &edges;

    EdgeFilter(SpeciesTree &st, vector<std::pair<int,int> > &edges) :
      st(st), edges(edges) { }

    bool should_ignore(spscp_pair ddecs[4]) {
      vector<std::pair<int,int> >::iterator it = edges.begin();
      vector<std::pair<int,int> >::iterator end = edges.end();
      for (; it != end; it++) {
        SpeciesNode *u = st[it->first], *v = st[it->second];
        for (spscp_pair *dec = ddecs; dec < ddecs + 4; ++dec) {
          SpeciesNode
            *root = st[dec->root],
            *top = st[dec->top],
            *bottom = st[dec->bottom];
          if (/* is u in the right subtree/subtree-complement? */
              (dec->is_scp ^ root->is_ancestor_of(u)) &&
              /* is v on the path? */
              (top->is_ancestor_of(v) && v->is_ancestor_of(bottom)))
            return true;
        }
      }
      return false;
    }
  };
}
