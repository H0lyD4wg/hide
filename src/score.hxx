#include <string.h>

namespace hgt {
  float **alloc_matrix(int side) {
    float *data = new float[side*side];
    memset(data, 0, sizeof(float)*side*side);
    float **rows = new float*[side];
    for (int i=0; i<side; ++i)
      rows[i] = data + i*side;
    return rows;
  }

  void delete_matrix(float **rows) {
    delete[] rows[0];
    delete[] rows;
  }
}
