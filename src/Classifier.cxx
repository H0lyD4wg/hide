#include "Classifier.hxx"

namespace hgt {
  bool Classifier::is_conflicting(QuartetTree qt) {
    SpeciesNode *a, *b, *c, *d;
    assign_pairings(qt, a, b, c, d);
    SpeciesNode *AB = species_tree.get_lca(a, b);
    SpeciesNode *CD = species_tree.get_lca(c, d);
  
    return ((AB->is_ancestor_of(c) || AB->is_ancestor_of(d)) &&
            (CD->is_ancestor_of(a) || CD->is_ancestor_of(b)));
  }

  /* GENerate DECoarationS : extract the appropriate tree decorations
   * from \param{qt}. Puts the resulting SP/SCP pairs in \param{ddecs}
   * (Directed DECorationS) and the PP pairs in \param{sdecs}
   * (Symmetric DECorationS). The caller is responsible to allocate
   * enough space for the decorations: four SP/SCP pairs and two PP pairs.
   */
  void Classifier::gen_decs(QuartetTree qt, spscp_pair *ddecs, pp_pair *sdecs) {
    SpeciesNode *EFG[3];
    get_internal_nodes(qt, EFG);
    // give the (now unique-sorted) internal nodes convenient names.
    SpeciesNode *E = EFG[0], *F = EFG[1];
    // we don't do ``*G = EFG[2];'' because we aren't going to use it.
    SpeciesNode *a, *b, *c, *d, *temp;
    assign_pairings(qt, a, b, c, d);
    if (!E->is_ancestor_of(a)) { temp = a; a = b; b = temp; }
    if (!E->is_ancestor_of(c)) { temp = c; c = d; d = temp; }
    SpeciesNode *A = E->step(a), *B = F->step(b),
                *C = E->step(c), *D = F->step(d);
    make_tree(qt, ddecs[0].is_scp, ddecs[0].root, A, a);
    make_tree(qt, ddecs[1].is_scp, ddecs[1].root, B, b);
    make_tree(qt, ddecs[2].is_scp, ddecs[2].root, C, c);
    make_tree(qt, ddecs[3].is_scp, ddecs[3].root, D, d);
    
    make_path(qt, ddecs[0].top, ddecs[0].bottom, B, b);
    make_path(qt, ddecs[1].top, ddecs[1].bottom, A, a);
    make_path(qt, ddecs[2].top, ddecs[2].bottom, D, d);
    make_path(qt, ddecs[3].top, ddecs[3].bottom, C, c);
    
    sdecs[0].ltop = ddecs[0].top;
    sdecs[0].lbottom = ddecs[0].bottom;
    sdecs[0].rtop = ddecs[1].top;
    sdecs[0].rbottom = ddecs[1].bottom;
    sdecs[1].ltop = ddecs[2].top;
    sdecs[1].lbottom = ddecs[2].bottom;
    sdecs[1].rtop = ddecs[3].top;
    sdecs[1].rbottom = ddecs[3].bottom;
  }

  void Classifier::make_path(QuartetTree qt,
                             int &top, int &bottom,
                             SpeciesNode *X, SpeciesNode *x) {
    SpeciesNode *y = species_tree[qt.pair_of(x->idx)];
    if (X->is_ancestor_of(y)) {
      top = species_tree.get_lca(y, x)->step(x)->idx;
      bottom = x->idx;
    } else {
      top = X->idx;
      bottom = x->idx;
    }
  }

  void Classifier::make_tree(QuartetTree qt,
                             bool &is_scp, int &root,
                             SpeciesNode *X, SpeciesNode *x) {
    SpeciesNode *y = species_tree[qt.pair_of(x->idx)];
    if (X->is_ancestor_of(y)) {
      is_scp = true;
      root = X->step(y)->idx;
    } else {
      is_scp = false;
      root = X->idx;
    }
  }

  /* find the internal nodes of the quartet tree \param{qt} and put them
   * in \param{EFG} in topologically-sorted order (that is, no node is an
   * ancestor of it's succesor).
   */
  void Classifier::get_internal_nodes(QuartetTree qt, SpeciesNode *EFG[3]) {
    SpeciesNode *leafs[4];
    EFG[0] = EFG[1] = EFG[2] = NULL;
    for (int i=0; i<4; ++i) leafs[i] = species_tree[qt[i]];
  
    for (int i=0; i<3; ++i) {
      for (int j=i+1; j<4; ++j) {
        // insert LCA(i,j) into EFG. make sure that EFG does not
        // contain any duplicate nodes and that it remains
        // topologically sorted.
        // when finished, EFG[2] (aka G) is LCA(E,F)=LCA(leafs)
        // and F /may/ be an ancestor of E.
        SpeciesNode *node = species_tree.get_lca(leafs[i], leafs[j]);
        for (int k=0; k<3; ++k) {
          if (NULL == EFG[k]) { EFG[k] = node; break; }
          else if (EFG[k] == node) break;
          else if (!EFG[k]->is_ancestor_of(node)) continue;
          else if (EFG[k]->is_ancestor_of(node)) {
            // push that node aside
            for (int l=2; l>k; --l) EFG[l] = EFG[l-1];
            // and take it's place!
            EFG[k] = node;
            break;
          } else { /* should be impossible */ }
        }
      }
    }
  }

  qtree_topology Classifier::classify(QuartetTree qt) {
    SpeciesNode *EFG[3];
    get_internal_nodes(qt, EFG);
  
    // give the (now unique-sorted) internal nodes convenient names.
    SpeciesNode *E = EFG[0], *F = EFG[1], *G = EFG[2];
    SpeciesNode
      *a = species_tree[qt[0]],
      *b = species_tree[qt[1]],
      *c = species_tree[qt[2]],
      *d = species_tree[qt[3]];
  
    if (!F->is_ancestor_of(E)) return TYPE_1;
    else if (E == species_tree.get_lca(a, b)) return TYPE_2LL;
    else if (E == species_tree.get_lca(c, d)) return TYPE_2RR;
    else if (F == species_tree.get_lca(a, b)) return TYPE_2LR;
    else if (G == species_tree.get_lca(a, b)) return TYPE_2RL;
    else return TYPE_1; // shouldn't happen.
  }

  void Classifier::assign_pairings(QuartetTree &qt,
                                   SpeciesNode *&a,
                                   SpeciesNode *&b,
                                   SpeciesNode *&c,
                                   SpeciesNode *&d) {
    int _a, _b, _c, _d;
    qt.pairings(_a, _b, _c, _d);
    a = species_tree[_a];
    b = species_tree[_b];
    c = species_tree[_c];
    d = species_tree[_d];
  }
}
