#include "QuartetTree.hxx"

namespace hgt {
  QuartetTree::QuartetTree() {
    // ensure that we can easily tell apart QuartetTrees that were not
    // initialised by qtree. negative valus are illegal everywhere.
    pair = weight = -1;
    for (int i=0; i<4; ++i) leafs[i] = -1;
  }

  int QuartetTree::pair_of(int idx) {
    int a, b, c, d;
    pairings(a, b, c, d);

    return
      (idx == a) ? b :
      (idx == b) ? a :
      (idx == c) ? d :
      (idx == d) ? c :
      -1;
  }

  void QuartetTree::pairings(int &a, int &b, int &c, int &d) {
    a = leafs[0];
    b = leafs[pair];
    c = leafs[(pair != 1) ? 1 : 2];
    d = leafs[(pair != 3) ? 3 : 2];
  }

  /* if \param{raw} belongs to \param{species} (that is, doesn't contain
   * any unrecognized names) initialise \param{qt} to the canonical form
   * of \param{raw} and return true. otherwise return false.
   */
  bool QuartetTree::init(const int leafs[4], int weight) {
    for (int i=0; i<4; ++i) this->leafs[i] = leafs[i];
  
    // bubble sort
    int temp;
    for (int i=2; i>=0; --i)
      for (int j=0; j<=i; ++j)
        if (this->leafs[j] > this->leafs[j+1]) {
          temp = this->leafs[j];
          this->leafs[j] = this->leafs[j+1];
          this->leafs[j+1] = temp; }
  
    // find the pairing
    int pair =
      this->leafs[0] == leafs[0] ? leafs[1] :
      this->leafs[0] == leafs[1] ? leafs[0] :
      this->leafs[0] == leafs[2] ? leafs[3] :
      this->leafs[0] == leafs[3] ? leafs[2] :
      -1;
    for (int i=1; i<4; ++i)
      if (this->leafs[i] == pair)
        this->pair = i;
  
    this->weight = weight;
  
    return true;
  }
}
