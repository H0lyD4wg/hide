#pragma once
#include <vector>
#include <string>
#include <map>
#include "rmq.h"

namespace hgt {
  class SpeciesInternalNode;
  class SpeciesLeafNode;
  
  class SpeciesNode {
  public:
    int idx, range_left, range_right;
    SpeciesNode *parent;
    SpeciesNode(int idx) : idx(idx)
    { parent = NULL; range_left = range_right = -1; }
    virtual SpeciesInternalNode* get_internal() { return NULL; }
    virtual SpeciesLeafNode* get_leaf() { return NULL; }
  
    /* returns a neighbour on the path from this to the target. */
    virtual SpeciesNode* step(SpeciesNode* target) { return NULL; }
  
    /* every node counts as it's own ancestor */
    bool is_ancestor_of(SpeciesNode* other) {
      return
        (range_left <= other->idx) &&
        (range_right >= other->idx);
    }
  };
  
  class SpeciesInternalNode : public SpeciesNode {
  public:
    SpeciesNode *left, *right;
    SpeciesInternalNode(int idx) : SpeciesNode(idx)
    { left = right = NULL; }
    virtual SpeciesInternalNode* get_internal() { return this; }
  
    virtual SpeciesNode* step(SpeciesNode* target) {
      int tidx = target->idx;
      if (tidx == idx) return this;
      else if ((tidx < idx) && (tidx >= range_left))
        return left;
      else if ((tidx > idx) && (tidx <= range_right))
        return right;
      else return parent;
    }
  };
  
  class SpeciesLeafNode : public SpeciesNode {
  public:
    const std::string *name;
    SpeciesLeafNode(int idx, const std::string _name) :
      SpeciesNode(idx) {
      name = new std::string(_name);
      range_left = range_right = idx;
    }
    ~SpeciesLeafNode() { delete name; }
    virtual SpeciesLeafNode* get_leaf() { return this; }
  
    virtual SpeciesNode* step(SpeciesNode* target) {
      if (target->idx == idx) return this;
      else return parent;
    }
  };


  class SpeciesTree {
  public:
    SpeciesTree() { rmqinfo = NULL; }
    ~SpeciesTree();
    SpeciesNode* operator[](const std::string &speciesname);
    SpeciesNode* operator[](int idx);
    SpeciesNode* get_lca(SpeciesNode *u, SpeciesNode *v);
    int root_idx, nodecount;
  
  public:
    // for finding a node by index:
    std::vector<SpeciesNode*> nodes;
    // for finding the LCA of two nodes by range-minimum query
    std::vector<unsigned int> depths;
    struct rmqinfo *rmqinfo;
    // for looking up a leaf by name
    struct strcompare {
      bool operator()(const std::string* s1, const std::string* s2) const
      { return s1->compare(*s2) < 0; } };
    std::map<const std::string*, SpeciesLeafNode*, strcompare> species;
  };
}