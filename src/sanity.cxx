#include <unittest++/UnitTest++.h>
#include <bitset>
#include <stdlib.h>
#include <stdio.h>

#include "SpeciesTree.hxx"
#include "Classifier.hxx"
#include "SymmetricDecoratedTree.hxx"
#include "AsymmetricDecoratedTree.hxx"
#include "EdgeFilter.hxx"
#include "ast.hxx"
#include "score.hxx"
#include "quartets.hxx"
#include "QuartetTreeIterator.hxx"
#include "rmq.h"

using namespace std;
using namespace hgt;

static const char pokemon_chunk[] =
"return \n"
"{{{'Bulbasaur', {'Ivysaur', 'Venosaur'}},\n"
"  {'Squirtle', {'Wartortle', 'Blastoise'}}},\n"
" {'Charmander', {'Charmeleon', 'Charizard'}}}\n";

void Pokemon(SpeciesTree &st) {
  lua_State *L = luaL_newstate();
  luaL_dostring(L, pokemon_chunk);
  SpeciesTreeFromTable(st, L);
  lua_close(L);
}

#include "tests/qtrees.cxx"
#include "tests/strees.cxx"
#include "tests/iterator.cxx"
#include "tests/bitset.cxx"

TEST(rmq) {
  /* Adapted from the unit test that Hideo Bannai distributes
   * with his code. To make the test run reasonably fast I'm
   * only checking a random sample of a 100 pairs rather than all
   * 500,000 of them. To make the test deterministic I'm also
   * seeding the rng with a constant rather than the time. */
  static const unsigned int N = 1000;
  static const unsigned int K = 100;
  VAL a[N];
  struct rmqinfo * ri;
  srandom(31337);
  for(unsigned int i=0; i<N; ++i)
    a[i] = random() % 10;
  ri = rm_query_preprocess(a, N);
  for(unsigned int k=0; k<K; ++k) {
    int i = random() % N;
    int j = random() % N;
    int x = rm_query_naive(a,i,j);
    int y = rm_query(ri, i, j);
    CHECK_EQUAL(x, y);
  }
  rm_free(ri);
}

int main() {
  return UnitTest::RunAllTests();
}
