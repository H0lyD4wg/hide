#include <stdlib.h>
#include <stdio.h>
#include "lua.hpp"

extern "C" { LUA_API int luaopen_hgt(lua_State *L); }
extern "C" { LUA_API int luaopen_lfs(lua_State *L); }
extern "C" { LUA_API int luaopen_lpeg(lua_State *L); }

extern char blob[];
extern int blob_size;

int main(int argc, char **argv){
  lua_State *lua = lua_open();
  luaL_openlibs(lua);
  luaopen_lfs(lua);
  luaopen_lpeg(lua);
  luaopen_hgt(lua);

  lua_createtable(lua, argc - 1, 0);
  for (int ii=1; ii < argc; ii++) {
    lua_pushstring(lua, argv[ii]);
    lua_rawseti(lua, -2, ii);
  }
  lua_setglobal(lua, "arg");

  luaL_dostring(lua, blob);
        
  return 0;
}
