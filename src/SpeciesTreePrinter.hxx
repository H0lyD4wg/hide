#pragma once
#include <iostream>
#include "SpeciesTree.hxx"

namespace hgt {
  class SpeciesTreePrinter {
  private:
    unsigned int depth;
    std::ostream &out;

  public:
    SpeciesTreePrinter(std::ostream &out) : out(out) { depth = 0; }

    void visit(SpeciesNode &node) {
      if (node.get_leaf()) return (*this)(*node.get_leaf());
      else return (*this)(*node.get_internal());
    }

    void operator()(SpeciesLeafNode &node) {
      for (unsigned int i=0; i<depth; ++i) out << "  ";
      out << node.idx << " - " << node.name->data() << std::endl;
    }

    void operator()(SpeciesInternalNode &node) {
      for (unsigned int i=0; i<depth; ++i) out << "  ";
      out << node.idx << std::endl;
      ++depth;
      visit(*node.left);
      visit(*node.right);
      --depth;
    }
  };
}
