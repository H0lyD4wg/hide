
#include "lua.hpp"
#ifdef _WINDLL
#define LUA_API __declspec(dllexport)
#endif
#ifndef LUA_API
#define LUA_API
#endif

extern "C" {
#include "luigi.h"
}

#include <sstream>
#include <iostream>
#include <cmath>
#include "SpeciesTree.hxx"
#include "SpeciesTreePrinter.hxx"
#include "QuartetTreeIterator.hxx"
#include "QuartetTree.hxx"
#include "AsymmetricDecoratedTree.hxx"
#include "SymmetricDecoratedTree.hxx"
#include "EdgeFilter.hxx"
#include "ast.hxx"
#include "score.hxx"

#ifdef _WINDLL
#include <float.h>
#include <stdio.h>
#define snprintf _snprintf
#define isnan _isnan
#endif

namespace hgt {
namespace lua {
  #define FINALIZER(N, T)  \
  static int N(lua_State *L) { \
    T **p = (T**)lua_touserdata(L, 1); \
    /*cout << "collected " << #T << "\t @" << (long)(*p) << endl;*/    \
    delete *p; \
    return 0; \
  }

  static int species_tree(lua_State *L) {
    luigi_argcheck(L, 1);
    if ((lua_type(L, 1) != LUA_TTABLE) &&
        (lua_type(L, 1) != LUA_TSTRING))
      return luaL_error(L, "argument should be a table or string,"
                           " but isn't.");
    SpeciesTree *st = new SpeciesTree();
    SpeciesTreeFromTable(*st, L);
    lua_remove(L, -1); // remove the argument
    luigi_push_udata(L, st, "hgt", "SpeciesTree");
    return 1;
  }

  static int st_get_node_count(lua_State *L) {
    luigi_argcheck(L, 1);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    lua_pushinteger(L, st.nodecount);
    return 1;
  }

  static void hgt_set_field(lua_State *L, const char *name, int idx) {
    lua_pushstring(L, name);
    lua_pushvalue(L, idx);
    lua_rawset(L, -3);
  }

  static void hgt_push_node(lua_State *L, SpeciesNode *node, int st) {
    luigi_push_udata(L, node, "hgt", "SpeciesNode");
    lua_newtable(L);
    hgt_set_field(L, "SpeciesTree", st);
    lua_setfenv(L, -2);
  }

  static int st_get_node(lua_State *L) {
    luigi_argcheck(L, 2);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    SpeciesNode *node = NULL;
    int idx = -1;
    switch (lua_type(L, 2)) {
    case LUA_TNUMBER:
      idx = luaL_checkint(L, 2);
      if ((idx < 0) || (idx >= st.nodecount)) { lua_pushnil(L); return 1; }
      node = st[idx];
      break;
    case LUA_TSTRING:
      node = st[string(luaL_checkstring(L, 2))];
      if (!node) { lua_pushnil(L); return 1; }
      break; }
    hgt_push_node(L, node, 1);
    return 1;
  }

  static int st_get_lca(lua_State *L) {
    luigi_argcheck(L, 3);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    SpeciesNode *u = *((SpeciesNode**)
                       luigi_checkudata
                       (L, 2, "hgt", "SpeciesNode"));
    SpeciesNode *v = *((SpeciesNode**)
                       luigi_checkudata
                       (L, 3, "hgt", "SpeciesNode"));
    SpeciesNode *lca = st.get_lca(u, v);
    hgt_push_node(L, lca, 1);
    return 1;
  }

  static int sn_eq(lua_State *L) {
    luigi_argcheck(L, 2);
    SpeciesNode *node = *((SpeciesNode**)
                          luigi_checkudata
                          (L, 1, "hgt", "SpeciesNode"));
    SpeciesNode *edon = *((SpeciesNode**)
                          luigi_checkudata
                          (L, 2, "hgt", "SpeciesNode"));
    lua_pushboolean(L, node == edon);
    return 1;
  }

  static int sn_pprint(lua_State *L) {
    luigi_argcheck(L, 1);
    SpeciesNode *node = *((SpeciesNode**)
                          luigi_checkudata
                          (L, 1, "hgt", "SpeciesNode"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 2);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 3, "hgt", "SpeciesTree"));
    if (node->get_leaf()) {
      char buffer[80];
      snprintf(buffer, 80, "%d-%s", node->idx,
               node->get_leaf()->name->data());
      lua_pushstring(L, buffer); }
    else {
      char buffer[80];
      snprintf(buffer, 80, "%d-LCA(%s,%s)", node->idx,
               st[node->range_left]->get_leaf()->name->data(),
               st[node->range_right]->get_leaf()->name->data());
      lua_pushstring(L, buffer); }
    return 1;
  }

  static int st_pprint(lua_State *L) {
    luigi_argcheck(L, 1);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    std::stringstream ss;
    SpeciesTreePrinter sp(ss);
    sp.visit(*(st[st.root_idx]));
    lua_pop(L, 1);
    lua_pushstring(L, ss.str().data());
    return 1;
  }

  static int qtree_pprint(lua_State *L) {
    luigi_argcheck(L, 1);
    QuartetTree &qt = **((QuartetTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "QuartetTree"));
    lua_pop(L, 1);
    char buffer[80];
    snprintf(buffer, 80, "<%d,%d,%d,%d|%d> (weight=%d)",
             qt[0], qt[1], qt[2], qt[3], qt.pair, qt.weight);
    lua_pushstring(L, buffer);
    return 1;
  }

  static int qtree_pprint_long(lua_State *L) {
    luigi_argcheck(L, 2);
    QuartetTree &qt = **((QuartetTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "QuartetTree"));
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 2, "hgt", "SpeciesTree"));
    char buffer[80];
    int a,b,c,d; qt.pairings(a,b,c,d);
    snprintf(buffer, 80, "[&WEIGHT=%d] ((%s, %s), (%s, %s));",
             qt.weight,
             st[a]->get_leaf()->name->data(),
             st[b]->get_leaf()->name->data(),
             st[c]->get_leaf()->name->data(),
             st[d]->get_leaf()->name->data());
    lua_pushstring(L, buffer);
    return 1;
  }

  static int st_weighted(lua_State *L) {
    int from = -1, count = -1;
    if (luigi_argcheck2(L, 2, 4) == 4) {
      from = luaL_checkint(L, 3);
      count = luaL_checkint(L, 4); }
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    luaL_checktype(L, 2, LUA_TTABLE);
    GenesQuartetTreeIterator *it = (from == -1)
      ? new GenesQuartetTreeIterator(st)
      : new GenesQuartetTreeIterator(st, from, count);

    luigi_push_udata(L, it, "hgt", "QuartetTreeIterator");
    lua_newtable(L);

    int length = lua_objlen(L, 2);
    for (int i=1; i<=length; ++i) {
      lua_rawgeti(L, 2, i);
      SpeciesTree *gt = *((SpeciesTree**)
                          luigi_checkudata
                          (L, -1, "hgt", "SpeciesTree"));
      it->add(gt);
      lua_rawseti(L, -2, i);
    }
    it->ready();

    hgt_set_field(L, "SpeciesTree", 1);
    lua_setfenv(L, -2);
    return 1;
  }

  static int st_all_qtrees(lua_State *L) {
    int from = -1, count = -1;
    if (luigi_argcheck2(L, 1, 3) == 3) {
      from = luaL_checkint(L, 2);
      count = luaL_checkint(L, 3); }
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    EnumQuartetTreeIterator *it = (from == -1)
      ? new EnumQuartetTreeIterator(st)
      : new EnumQuartetTreeIterator(st, from, count);
    luigi_push_udata(L, it, "hgt", "QuartetTreeIterator");
    lua_newtable(L);
    hgt_set_field(L, "SpeciesTree", 1);
    lua_setfenv(L, -2);
    return 1;
  }

  static int st_qtrees(lua_State *L) {
    luigi_argcheck(L, 2);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    luaL_checktype(L, 2, LUA_TTABLE);
    VectorQuartetTreeIterator *it = new VectorQuartetTreeIterator();
    QuartetTreesFromTable(st, it->arr, L);
    luigi_push_udata(L, it, "hgt", "QuartetTreeIterator");
    lua_newtable(L);
    hgt_set_field(L, "SpeciesTree", 1);
    lua_setfenv(L, -2);
    return 1;
  }

  class Scoreboard {
  public:
    SpeciesTree &st;
    float **scores;
    Scoreboard(SpeciesTree &st) : st(st)
    { scores = alloc_matrix(st.nodecount); }
    ~Scoreboard() { delete_matrix(scores); }
  };

  static void hgt_push_score(lua_State *L, Scoreboard *sb, int st) {
    luigi_push_udata(L, sb, "hgt", "Scoreboard");
    lua_newtable(L);
    lua_pushliteral(L, "SpeciesTree");
    lua_pushvalue(L, st);
    lua_rawset(L, -3);
    lua_setfenv(L, -2);
  }

  static int st_blank(lua_State *L) {
    luigi_argcheck(L, 1);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));

    Scoreboard *scores = new Scoreboard(st);
    for (int i=0; i<st.nodecount; ++i)
      for (int j=0; j<st.nodecount; ++j)
        scores->scores[i][j] = is_meaningful(st[i], st[j]) ? 0 : -1;

    hgt_push_score(L, scores, 1);
    return 1;
  }

  bool EdgesFromTable(std::vector<std::pair<int,int> > &edges,
                      lua_State *L) {
    if (!lua_istable(L, -1)) return false;
    int length = lua_objlen(L, -1);

    for (int i=1; i<=length; ++i) {
      lua_rawgeti(L, -1, i); // ith edge
      lua_rawgeti(L, -1, 1); // endpoint of ith edge
      lua_rawgeti(L, -2, 2); // the other endpoint
      SpeciesNode *u = *((SpeciesNode**)
                         luigi_checkudata
                         (L, -2, "hgt", "SpeciesNode"));
      SpeciesNode *v = *((SpeciesNode**)
                         luigi_checkudata
                         (L, -1, "hgt", "SpeciesNode"));
      edges.push_back(std::pair<int,int>(u->idx, v->idx));
      lua_pop(L, 3);
    }

    lua_remove(L, -1);
    return true;
  }

  static int st_score(lua_State *L) {
    bool ignore = 3 == luigi_argcheck2(L, 2, 3);
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 1, "hgt", "SpeciesTree"));
    QuartetTreeIterator &it = **((QuartetTreeIterator**)
                                 luigi_checkudata
                                 (L, 2, "hgt", "QuartetTreeIterator"));

    vector<std::pair<int,int> > edges;
    if (ignore) {
      if (EdgesFromTable(edges, L));
      else return luaL_error(L, "couldn't decipher edge-table."); }

    Classifier cf(st);
    AsymmetricDecoratedTree adt(st);
    SymmetricDecoratedTree sdt(st);
    EdgeFilter ef(st, edges);

    for (; !it.finished(); ++it) {
      if (!cf.is_conflicting(*it)) continue;
      spscp_pair ddecs[4];
      pp_pair sdecs[2];
      cf.gen_decs(*it, ddecs, sdecs);
      if (ef.should_ignore(ddecs)) continue;
      adt.hang((*it).weight, ddecs);
      sdt.hang((*it).weight, sdecs); }

    Scoreboard *dscores = new Scoreboard(st);
    Scoreboard *sscores = new Scoreboard(st);
    adt.compute_scores(dscores->scores);
    sdt.compute_scores(sscores->scores);

    hgt_push_score(L, dscores, 1);
    hgt_push_score(L, sscores, 1);

    return 2;
  }

  static int score_fold(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &dscores = **((Scoreboard**)
                             luigi_checkudata
                             (L, 1, "hgt", "Scoreboard"));
    Scoreboard &sscores = **((Scoreboard**)
                             luigi_checkudata
                             (L, 2, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(dscores.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
        if (i<j) scores->scores[i][j] =
                   dscores.scores[i][j] +
                   dscores.scores[j][i] -
                   sscores.scores[i][j];
        else scores->scores[i][j] = -1;
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_fold_max(lua_State *L) {
    luigi_argcheck(L, 1);
    Scoreboard &orig = **((Scoreboard**)
                          luigi_checkudata
                          (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 2);
    Scoreboard *scores = new Scoreboard(orig.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
        if (i<j) scores->scores[i][j] = // maximum
                   (orig.scores[i][j] > orig.scores[j][i]) ?
                    orig.scores[i][j] : orig.scores[j][i];
        else scores->scores[i][j] = -1;
    hgt_push_score(L, scores, 3);
    return 1;
  }

  static int score_keep_max(lua_State *L) {
    luigi_argcheck(L, 1);
    Scoreboard &orig = **((Scoreboard**)
                          luigi_checkudata
                          (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 2);
    Scoreboard *scores = new Scoreboard(orig.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=i+1; j<scores->st.nodecount; ++j)
        if (orig.scores[i][j] >= orig.scores[j][i])
          scores->scores[i][j] = orig.scores[i][j];
        else scores->scores[j][i] = orig.scores[j][i];
    hgt_push_score(L, scores, 3);
    return 1;
  }

  static int score_cutoff(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &orig = **((Scoreboard**)
                          luigi_checkudata
                          (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    double coff = luaL_checknumber(L, 2);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(orig.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
	if (orig.scores[i][j] == -1) scores->scores[i][j] = -1;
	else if (orig.scores[i][j] < coff) scores->scores[i][j] = 0;
        else scores->scores[i][j] = orig.scores[i][j];
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_expt(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &orig = **((Scoreboard**)
                          luigi_checkudata
                          (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    int expt = luaL_checkint(L, 2);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(orig.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
	if (orig.scores[i][j] == -1) scores->scores[i][j] = -1;
	else {
          double r = 1;
          double v = orig.scores[i][j];
          for (int k=0; k<expt; ++k) r = r*v;
          scores->scores[i][j] = r; }
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_div(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &num = **((Scoreboard**)
                         luigi_checkudata(L, 1, "hgt", "Scoreboard"));
    Scoreboard &denom = **((Scoreboard**)
                           luigi_checkudata(L, 2, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(num.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
	if (num.scores[i][j] == -1) scores->scores[i][j] = -1;
	else scores->scores[i][j] =
	       num.scores[i][j] / denom.scores[i][j];
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_mul(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &a = **((Scoreboard**)
                       luigi_checkudata(L, 1, "hgt", "Scoreboard"));
    Scoreboard &b = **((Scoreboard**)
                       luigi_checkudata(L, 2, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(a.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
	if (a.scores[i][j] == -1) scores->scores[i][j] = -1;
	else scores->scores[i][j] =
	       a.scores[i][j] * b.scores[i][j];
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_add(lua_State *L) {
    luigi_argcheck(L, 2);
    Scoreboard &left = **((Scoreboard**)
                          luigi_checkudata
                          (L, 1, "hgt", "Scoreboard"));
    Scoreboard &right = **((Scoreboard**)
                           luigi_checkudata
                           (L, 2, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 3);
    Scoreboard *scores = new Scoreboard(left.st);
    for (int i=0; i<scores->st.nodecount; ++i)
      for (int j=0; j<scores->st.nodecount; ++j)
	if (left.scores[i][j] == -1) scores->scores[i][j] = -1;
        else if (isnan(right.scores[i][j])) // left is privileged!
          scores->scores[i][j] = left.scores[i][j];
	else scores->scores[i][j] =
	       left.scores[i][j] + right.scores[i][j];
    hgt_push_score(L, scores, 4);
    return 1;
  }

  static int score_table(lua_State *L) {
    luigi_argcheck(L, 1);
    Scoreboard &scores = **((Scoreboard**)
                            luigi_checkudata
                            (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 2);
    lua_newtable(L);
    int count = 0;
    for (int i=0; i<scores.st.nodecount; ++i)
      for (int j=i+1; j<scores.st.nodecount; ++j)
	if (isnan(scores.scores[i][j]) ||
	    (scores.scores[i][j] == -1)) continue;
	else {
	  lua_createtable(L, 0, 3);
	  lua_pushliteral(L, "score");
	  lua_pushnumber(L, scores.scores[i][j]);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "u");
	  lua_pushcfunction(L, st_get_node);
          lua_pushvalue(L, 3);
	  lua_pushinteger(L, i);
	  lua_call(L, 2, 1);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "v");
	  lua_pushcfunction(L, st_get_node);
          lua_pushvalue(L, 3);
	  lua_pushinteger(L, j);
	  lua_call(L, 2, 1);
	  lua_rawset(L, 5);

	  lua_rawseti(L, 4, ++count);
	}
    return 1;
  }

  static int score_folded_table(lua_State *L) {
    luigi_argcheck(L, 1);
    Scoreboard &scores = **((Scoreboard**)
                            luigi_checkudata
                            (L, 1, "hgt", "Scoreboard"));
    lua_getfenv(L, 1);
    lua_pushliteral(L, "SpeciesTree");
    lua_rawget(L, 2);
    lua_newtable(L);
    int count = 0;
    for (int i=0; i<scores.st.nodecount; ++i)
      for (int j=i+1; j<scores.st.nodecount; ++j)
	if (isnan(scores.scores[i][j]) ||
	    (scores.scores[i][j] == -1)) continue;
	else {
	  lua_createtable(L, 0, 3);
	  lua_pushliteral(L, "score");
	  lua_pushnumber(L, scores.scores[i][j] + scores.scores[j][i]);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "lscore");
	  lua_pushnumber(L, scores.scores[i][j]);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "rscore");
	  lua_pushnumber(L, scores.scores[j][i]);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "u");
	  lua_pushcfunction(L, st_get_node);
          lua_pushvalue(L, 3);
	  lua_pushinteger(L, i);
	  lua_call(L, 2, 1);
	  lua_rawset(L, 5);

	  lua_pushliteral(L, "v");
	  lua_pushcfunction(L, st_get_node);
          lua_pushvalue(L, 3);
	  lua_pushinteger(L, j);
	  lua_call(L, 2, 1);
	  lua_rawset(L, 5);

	  lua_rawseti(L, 4, ++count);
	}
    return 1;
  }

  static int score_get(lua_State *L) {
    luigi_argcheck(L, 3);
    Scoreboard &scores = **((Scoreboard**)
                            luigi_checkudata
                            (L, 1, "hgt", "Scoreboard"));
    int i = luaL_checkint(L, 2)-1;
    int j = luaL_checkint(L, 3)-1;
    lua_pushnumber(L, scores.scores[i][j]);
    return 1;
  }

  static int iter_next(lua_State *L) {
    luigi_argcheck(L, 3); // what are the other two?
    QuartetTreeIterator &it = **((QuartetTreeIterator**)
                                 luigi_checkudata
                                 (L, 1, "hgt", "QuartetTreeIterator"));
    lua_pop(L, 3);
    if (it.finished()) lua_pushnil(L);
    else {
      QuartetTree *qt = new QuartetTree(*it);
      luigi_push_udata(L, qt, "hgt", "QuartetTree");
      ++it; }
    return 1;
  }

  static int iter_match(lua_State *L) {
    luigi_argcheck(L, 2);
    QuartetTreeIterator &it = **((QuartetTreeIterator**)
                                 luigi_checkudata
                                 (L, 1, "hgt", "QuartetTreeIterator"));
    SpeciesTree &st = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 2, "hgt", "SpeciesTree"));
    MatchQuartetTreeIterator *r = new MatchQuartetTreeIterator(it, st);
    luigi_push_udata(L, r, "hgt", "QuartetTreeIterator");
    lua_newtable(L);
    hgt_set_field(L, "UnderlayingIterator", 1);
    lua_setfenv(L, -2);
    return 1;
  }

  static int iter_bootstrap(lua_State *L) {
    luigi_argcheck(L, 2);
    QuartetTreeIterator &it = **((QuartetTreeIterator**)
                                 luigi_checkudata
                                 (L, 1, "hgt", "QuartetTreeIterator"));
    int treshold = luaL_checkint(L, 2);
    BootstrapQuartetTreeIterator *r =
      new BootstrapQuartetTreeIterator(it, treshold);
    luigi_push_udata(L, r, "hgt", "QuartetTreeIterator");
    lua_newtable(L);
    hgt_set_field(L, "UnderlayingIterator", 1);
    lua_setfenv(L, -2);
    return 1;
  }

  static int iter_translate(lua_State *L) {
    luigi_argcheck(L, 3);
    QuartetTreeIterator &it = **((QuartetTreeIterator**)
                                 luigi_checkudata
                                 (L, 1, "hgt", "QuartetTreeIterator"));
    SpeciesTree &from = **((SpeciesTree**)
                           luigi_checkudata
                           (L, 2, "hgt", "SpeciesTree"));
    SpeciesTree &to = **((SpeciesTree**)
                         luigi_checkudata
                         (L, 3, "hgt", "SpeciesTree"));
    TranslatedQuartetTreeIterator *r =
      new TranslatedQuartetTreeIterator(it, from, to);
    luigi_push_udata(L, r, "hgt", "QuartetTreeIterator");
    lua_newtable(L);
    hgt_set_field(L, "UnderlayingIterator", 1);
    hgt_set_field(L, "From", 2);
    hgt_set_field(L, "To", 3);
    lua_setfenv(L, -2);
    return 1;
  }

  static const luaL_Reg hgt_f[] = {
    {"species_tree", species_tree},
    {NULL, NULL}};

  FINALIZER(st_gc, SpeciesTree);
  static const luaL_Reg speciestree_m[] = {
    {"__gc", st_gc},
    {"__tostring", st_pprint},
    {"node", st_get_node},
    {"lca", st_get_lca},
    {"node_count", st_get_node_count},
    {"weighted_quartets", st_weighted},
    {"all_quartets", st_all_qtrees},
    {"qtree_set", st_qtrees},
    {"score", st_score},
    {"blank_scoreboard", st_blank},
    {NULL, NULL}};

  static const luaL_reg speciesnode_m[] = {
    {"__tostring", sn_pprint},
    {"__eq", sn_eq},
    {NULL, NULL}};

  FINALIZER(iter_gc, QuartetTreeIterator);
  static const luaL_Reg iter_m[] = {
    {"__gc", iter_gc},
    {"__call", iter_next},
    {"filter_match", iter_match},
    {"bootstrap", iter_bootstrap},
    {"translate", iter_translate},
    {NULL, NULL}};

  FINALIZER(qtree_gc, QuartetTree);
  static const luaL_Reg qtree_m[] = {
    {"__gc", qtree_gc},
    {"__tostring", qtree_pprint},
    {"format_long", qtree_pprint_long},
    {NULL, NULL}};

  FINALIZER(score_gc, Scoreboard);
  static const luaL_Reg score_m[] = {
    {"__gc", score_gc},
    {"fold", score_fold},
    {"fold_max", score_fold_max},
    {"keep_max", score_keep_max},
    {"cutoff", score_cutoff},
    {"expt", score_expt},
    {"__div", score_div},
    {"__mul", score_mul},
    {"__add", score_add},
    {"table", score_table},
    {"folded_table", score_folded_table},
    {"get", score_get},
    {NULL, NULL}};

  LUALIB_API int luaopen(lua_State *L) {
    luaL_register(L, "hgt", hgt_f);

    lua_pushliteral(L, "hgt");
    lua_newtable(L);

    luigi_create_metatable(L, "SpeciesTree", speciestree_m);
    luigi_create_metatable(L, "SpeciesNode", speciesnode_m);
    luigi_create_metatable(L, "QuartetTreeIterator", iter_m);
    luigi_create_metatable(L, "QuartetTree", qtree_m);
    luigi_create_metatable(L, "Scoreboard", score_m);

    lua_rawset(L, LUA_REGISTRYINDEX);

    return 1;
  }
}
}

extern "C" { LUA_API int luaopen_hgt(lua_State *L); }
LUA_API int luaopen_hgt(lua_State *L) {
  return hgt::lua::luaopen(L);
}
