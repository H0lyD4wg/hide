
#include "SymmetricDecoratedTree.hxx"

namespace hgt {
  void SymmetricDecoratedTree::hang(int weight, pp_pair p) {
    path[p.rbottom].push_back(symmetric_triple(weight, p.ltop, p.lbottom));
    path[st[p.rtop]->parent->idx]
      .push_back(symmetric_triple(weight, p.ltop, p.lbottom));
  
    path[p.lbottom].push_back(symmetric_triple(weight, p.rtop, p.rbottom));
    path[st[p.ltop]->parent->idx]
      .push_back(symmetric_triple(weight, p.rtop, p.rbottom));
  }

  void SymmetricDecoratedTree::augment(int vidx, int *val) {
    vector<int> counter;
    counter.assign(st.nodecount, 0);
  
    deque<symmetric_triple>::iterator it;
    for (it = path[vidx].begin(); it != path[vidx].end(); it++) {
      counter[it->bottom_idx] += it->weight;
      counter[st[it->top_idx]->parent->idx] -= it->weight;
    }
  
    compute_vals(st.root_idx, counter, val);
  }
  
  // helper method used by ``augment''
  void SymmetricDecoratedTree::compute_vals(int uidx,
                                            vector<int> &counter,
                                            int *val) {
    SpeciesInternalNode *u = st[uidx]->get_internal();
    val[uidx] = counter[uidx];
    if (u) {
      compute_vals(u->left->idx, counter, val);
      compute_vals(u->right->idx, counter, val);
      val[uidx] += val[u->left->idx] + val[u->right->idx];
    }
  }
}