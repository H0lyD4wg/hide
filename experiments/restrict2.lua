#!/usr/bin/env lua

require 'io'
require 'math'
require 'gsl'

require 'parser'
require 'lpeg'

local ifile = io.open(arg[1])
local text = ifile:read('*a')
ifile:close()
local species, genes =
   lpeg.match(space * nwtree * space * lpeg.Ct(nwtree^0), text)
local mean = tonumber(arg[2])
local stdev = tonumber(arg[3])

function enumerate_leafs_h(tree, leafs)
  if type(tree) == 'string' then
    leafs[#leafs+1] = tree
    return end
  enumerate_leafs_h(tree[1], leafs)
  enumerate_leafs_h(tree[2], leafs) end

function enumerate_leafs(tree)
  local leafs = {} enumerate_leafs_h(tree, leafs)
  return leafs end

function restrict(tree, leafs)
  if type(tree) == 'string' then return leafs[tree] and tree
  elseif type(tree) == 'table' then
  local left = restrict(tree[1], leafs)
  local right = restrict(tree[2], leafs)
  if left and right then return {left, right}
  else return left or right end end end

function pprint(tree, out)
  if type(tree) == 'string' then out:write(tree)
  elseif type(tree) == 'table' then
     out:write('(')
     pprint(tree[1], out)
     out:write(', ')
     pprint(tree[2], out)
     out:write(')')
  end end

local out = io.stdout
pprint(species, out) out:write(';\n\n') out:flush()
local rng = gsl.rng('mt19937', 31337)
local leafs = enumerate_leafs(species)

for i, gene in ipairs(genes) do
  local nleafs = rng:get_normal_int(mean, stdev)
  nleafs = math.min(nleafs, 50)
  local lhash = rng:choose(leafs, nleafs)
  for i, foo in ipairs(lhash) do lhash[foo] = 1 end
  gene = restrict(gene, lhash)
  pprint(gene, out) out:write(';\n')
end
