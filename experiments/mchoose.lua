#!/usr/bin/env lua

require 'io'
require 'gsl'
require 'genutils'
require 'lpeg'

inhibit = true
require 'score'
inhibit = nil

-- Functions that we're going to use:
function choose_highways(rng, tree, count)
   local nodes = tree.nodes
   local edges
   repeat
      edges = {}
      for i = 1, count do
         local u, v = random_horizontal_edge(rng, nodes)
         if u.index > v.index then u, v = v, u end
         edges[#edges + 1] = {u, v}
      end
   until (not common_endpoints(edges)) and (not conflicting(tree, edges))
   return edges
end

function common_endpoints(edges)
   local nodes = {}
   for i, edge in ipairs(edges) do
      u, v = unpack(edge)
      for j, node in ipairs(nodes) do
         if node == u or node == v then return true end end
      nodes[#nodes + 1] = u
      nodes[#nodes + 1] = v
   end
   return false
end

function conflicting(tree, edges)
   for perm in permutations(edges) do
      for flip in flippings(perm) do
         local gene = make_gene(tree)
         for i, edge in ipairs(flip) do
            local u = gene.nodes[edge[1].index]
            local v = gene.nodes[edge[2].index]
            -- did one of the endpoints die in a previous hgt?
            if not u or not v then return true end
            if not valid_edge(u, v) then return true end
            hgt(gene, u, v)
         end
      end
   end
   return false
end


local dir = arg[1] .. '/'
local count = tonumber(arg[2])
local seeds = loadstring('return ' .. (arg[3] or '{}'))()

for i, seed in ipairs(seeds) do
   local ifile = io.open(dir .. 'tree' .. i .. '.newick')
   local species = ifile:read('*a') ifile:close()
   species = nwtree_parse(species)
   species = make_species(species)
   local rng = gsl.rng('mt19937', seed)
   local edges = choose_highways(rng, species, count)
   print('tree ' .. i .. ' edges :')
   for j, edge in ipairs(edges) do
      local u, v = unpack(edge)
      print('', str(u) .. ' --> ' .. str(v)) end
end
