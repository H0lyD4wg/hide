#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>
#include "luigi.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_permutation.h>
#include <math.h>

static int lgsl_rng(lua_State *L) {
  const gsl_rng_type *t = gsl_rng_mt19937;
  unsigned long int seed = 0;
  /* TODO: derive rng type from 1st argument */
  if (lua_gettop(L) >= 2) seed = luaL_checkint(L, 2);
  gsl_rng *rng = gsl_rng_alloc(t);
  gsl_rng_set(rng, seed);
  luigi_push_udata(L, rng, "gsl", "rng");
  return 1; }
static int lgsl_rng_uniform_int(lua_State *L) {
  luigi_argcheck(L, 2);
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  unsigned long int n = luaL_checkint(L, 2);
  lua_pushinteger(L, 1+gsl_rng_uniform_int(*rng, n));
  return 1; }
static int lgsl_rng_gaussian_int(lua_State *L) {
  luigi_argcheck(L, 3);
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  unsigned long int mean = luaL_checkint(L, 2);
  unsigned long int stdev = luaL_checkint(L, 3);
  double dev = gsl_ran_gaussian(*rng, (double)stdev);
  lua_pushinteger(L, mean+round(dev));
  return 1; }
static int lgsl_rng_choose(lua_State *L) {
  luigi_argcheck(L, 3);
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  luaL_checktype(L, 2, LUA_TTABLE);
  int k = luaL_checkint(L, 3);
  int n = lua_objlen(L, 2);
  int *src = calloc(n, sizeof(int));
  int *dest = calloc(k, sizeof(int));
  int i;
  for (i=0; i<n; ++i) src[i] = i+1;
  gsl_ran_choose(*rng, dest, k, src, n, sizeof(int));
  free(src);
  lua_createtable(L, k, 0);
  for (i=1; i<=k; ++i) {
    lua_rawgeti(L, 2, dest[i-1]);
    lua_rawseti(L, 4, i); }
  free(dest);
  return 1; }
static int lgsl_rng_sample(lua_State *L) {
  luigi_argcheck(L, 3);
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  luaL_checktype(L, 2, LUA_TTABLE);
  int k = luaL_checkint(L, 3);
  int n = lua_objlen(L, 2);
  int *src = calloc(n, sizeof(int));
  int *dest = calloc(k, sizeof(int));
  int i;
  for (i=0; i<n; ++i) src[i] = i+1;
  gsl_ran_sample(*rng, dest, k, src, n, sizeof(int));
  free(src);
  lua_createtable(L, k, 0);
  for (i=1; i<=k; ++i) {
    lua_rawgeti(L, 2, dest[i-1]);
    lua_rawseti(L, 4, i); }
  free(dest);
  return 1; }
static int lgsl_rng_shuffle(lua_State *L) {
  luigi_argcheck(L, 2);
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  luaL_checktype(L, 2, LUA_TTABLE);
  int n = lua_objlen(L, 2);
  int *base = calloc(n, sizeof(int));
  int i;
  for (i=0; i<n; ++i) base[i] = i+1;
  gsl_ran_shuffle(*rng, base, n, sizeof(int));
  lua_createtable(L, n, 0);
  for (i=1; i<=n; ++i) {
    lua_rawgeti(L, 2, base[i-1]);
    lua_rawseti(L, 3, i); }
  free(base);
  return 1; }
static int lgsl_rng_gc(lua_State *L) {
  gsl_rng **rng = luigi_checkudata(L, 1, "gsl", "rng");
  gsl_rng_free(*rng);
  return 0;
}

static int lgsl_permutation(lua_State *L) {
  luigi_argcheck(L, 1);
  int n = luaL_checkint(L, 1);
  gsl_permutation *perm = gsl_permutation_calloc(n);
  luigi_push_udata(L, perm, "gsl", "permutation");
  return 1; }
static int lgsl_perm_gc(lua_State *L) {
  gsl_permutation **perm = luigi_checkudata(L, 1, "gsl", "permutation");
  gsl_permutation_free(*perm);
  return 0;
}
static int lgsl_perm_get(lua_State *L) {
  luigi_argcheck(L, 2);
  gsl_permutation **perm = luigi_checkudata(L, 1, "gsl", "permutation");
  int k = luaL_checkint(L, 2) - 1;
  if (k >= gsl_permutation_size(*perm)) return 0;
  lua_pushinteger(L, gsl_permutation_get(*perm, k)+1);
  return 1;
}
static int lgsl_perm_next(lua_State *L) {
  luigi_argcheck(L, 1);
  gsl_permutation **perm = luigi_checkudata(L, 1, "gsl", "permutation");
  if (GSL_SUCCESS == gsl_permutation_next(*perm)) return 1;
  else return 0;
}


static const luaL_Reg gsl_f[] = {
  {"rng", lgsl_rng},
  {"permutation", lgsl_permutation},
  {NULL, NULL}};

static const luaL_Reg rng_m[] = {
  {"__gc", lgsl_rng_gc},
  {"get_int", lgsl_rng_uniform_int},
  {"get_normal_int", lgsl_rng_gaussian_int},
  {"choose", lgsl_rng_choose},
  {"sample", lgsl_rng_sample},
  {"shuffle", lgsl_rng_shuffle},
  {NULL, NULL}};


static const luaL_Reg perm_m[] = {
  {"__gc", lgsl_perm_gc},
  {"get", lgsl_perm_get}, /* Ideally __index, but I'm too lazy. */
  {"next", lgsl_perm_next},
  {NULL, NULL}};



extern int luaopen_gsl(lua_State *L);
int luaopen_gsl(lua_State *L) {
  luaL_register(L, "gsl", gsl_f);

  lua_pushliteral(L, "gsl");
  lua_newtable(L);

  luigi_create_metatable(L, "rng", rng_m);
  luigi_create_metatable(L, "permutation", perm_m);

  lua_rawset(L, LUA_REGISTRYINDEX);

  return 1;
}
