#!/usr/bin/env lua

require 'hgt'
require 'io'

inhibit = true
require 'score'
inhibit = nil

-- all this juggling attempts to replicate the behavior of load_species_genes from the removed "common" module
local genes = load_genes(arg[1])
local species = genes[1]
genes[1] = genes[#genes]
genes[#genes] = nil

local niter = tonumber(arg[2])
local ntop = tonumber(arg[3])
local hws = loadstring('return' .. (arg[4] or '{}'))()

local chunk_length = 500000

for i, hw in ipairs(hws) do
   hws[i] = {species:node(hw[1]-1), species:node(hw[2]-1)}
end

local ignore = {}
for i = 1, niter do
   print('# Iteration '..i)
   print('# Ignored edges:')
   for j, edge in ipairs(ignore) do
      print('\t' .. tostring(edge[1]) .. ' --> ' .. tostring(edge[2]))
   end
   
   local scores = species:blank_scoreboard()
   for i, gene in ipairs(genes) do
      local raw = species:blank_scoreboard()
      local norm = species:blank_scoreboard()
      for i = 1, qcount(gene), chunk_length do
         raw = raw + species:score(gene:
                                   all_quartets(i-1, chunk_length):
                                   filter_match(gene):
                                   translate(gene, species), ignore)
         norm = norm + species:score(gene:
                                     all_quartets(i-1, chunk_length):
                                     translate(gene, species))
         collectgarbage('collect')
      end
      scores = scores + (raw/norm):keep_max():expt(2) end
   
   local hiscores = scores:fold(species:blank_scoreboard()):table()
   table.sort(hiscores, function (a, b) return a.score > b.score end)
   local remove = nil
   for j = 1, ntop do
      for k, hw in ipairs(hws) do
         local u, v = hiscores[j].u, hiscores[j].v
         if u == hw[1] and v == hw[2] then remove = hw  break end end
      if remove then break end end
   if remove then
      print('# Removed edge ' .. tostring(remove[1]) ..
         ' --> ' .. tostring(remove[2]), '(true highway)')
   else
      local u, v = hiscores[1].u, hiscores[1].v
      remove = {u, v}
      print('# Removed edge ' .. tostring(remove[1]) ..
         ' --> ' .. tostring(remove[2]), '(false positive)')
   end
   ignore[#ignore+1] = remove
   ignore[#ignore+1] = {remove[2], remove[1]}
   
   print('# Scores:')
   print_folded_scores{scores=scores, count=15}
end
