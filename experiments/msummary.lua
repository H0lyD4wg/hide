#!/usr/bin/env lua

require 'io'
require 'string'

local dir = arg[1]
local ext = arg[2] or 'scores'
local ifile = io.open(dir .. '/chosen.txt')
local hgts = {}

local i = nil
local edges = nil
for line in ifile:lines() do
   local ix = tonumber(string.match(line, "tree (%d+) edges :"))
   i = ix or i
   if ix then edges = {} hgts[i] = edges end
   local u, v = string.match(line, "\t(%d+)%-.* (%d+)%-")
   if u and v then edges[#edges + 1] =  line end
end

local function where(lines, iter)
   local ret = {}
   for j, line in ipairs(lines) do ret[j] = 'XXX' end
   local i = 0;
   for edge in iter do
      i = i+1
      local u, v = string.match(edge, "(%d+)%-.* (%d+)%-")
      for j, line in ipairs(lines) do
         local x, y = string.match(line, "(%d+)%-.* (%d+)%-")
         if (x == u) and (y == v) then ret[j] = i end end
   end
   return ret
end

local dummy = { lines = function (self) return function(x, y, z) return nil end end }

for i, edges in ipairs(hgts) do
   local ifile = io.open(dir .. '/set' .. i .. '.' .. ext) or dummy
   print('set '..i..':', unpack(where(edges, ifile:lines()))) end
