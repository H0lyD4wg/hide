#!/usr/bin/env lua

require 'io'
require 'string'

local ifile = io.open(arg[1])
local n = tonumber(arg[2])
local width = tonumber(arg[3] or 100)
local split = tonumber(arg[4] or width/2)

io.stdout:write('"{')
local i = nil
for line in ifile:lines() do
   local ix = tonumber(string.match(line, "tree (%d+) edges :"))
   i = ix or i
   if i == n then
      local u, v = string.match(line, "\t(%d+)%-.* (%d+)%-")
      if u and v then
         io.stdout:write(string.format(
                            "{%d, %d; width=%d, split=%d},",
                            u+1, v+1, width, split)) end end
end
io.stdout:write('}"')
