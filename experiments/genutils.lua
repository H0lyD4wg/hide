require 'gsl'
require 'math'
require 'coroutine'
require 'hgt'
species_tree = hgt.species_tree

-- Tree manipulation
local function link(tree, parent)
  if type(tree) == 'string' then
     return {name = tree, parent = parent}
  elseif type(tree) == 'table' then
     tree.parent = parent
     tree[1] = link(tree[1], tree)
     tree[2] = link(tree[2], tree)
     return tree end end

function make_species(tree)
   local result = {}
   result.root = link(tree, result)
   result[1] = result.root
   result.nodes = enumerate_nodes(result.root)
   for i, node in ipairs(result.nodes) do node.index = i end
   return result end

local function deepcopy(tree, parent)
  if not tree then return nil end
  local result = {parent = parent, name = tree.name, index = tree.index}
  result[1] = deepcopy(tree[1], result)
  result[2] = deepcopy(tree[2], result)
  return result end

function make_gene(tree)
  local result = {}
  result.root = deepcopy(tree.root, result)
  result[1] = result.root
  result.nodes = enumerate_nodes(result.root)
  return result end

local function enumerate_nodes_h(tree, nodes)
   if not tree then return end
   enumerate_nodes_h(tree[1], nodes)
   nodes[#nodes+1] = tree
   enumerate_nodes_h(tree[2], nodes) end

function enumerate_nodes(tree)
   local nodes = {}
   enumerate_nodes_h(tree, nodes)
   return nodes end

function valid_edge(u, v)
   -- are u and v siblings?
   if u.parent == v.parent then return false end
   -- is u an ancestor of v?
   local x = v
   while x do
      if u == x then return false end
      x = x.parent
   end
   -- is v an ancestor of u?
   local x = u
   while x do
      if v == x then return false end
      x = x.parent
   end
   return true end

function random_horizontal_edge(rng, nodes)
  local u, v
  repeat
    u = nodes[rng:get_int(#nodes)]
    v = nodes[rng:get_int(#nodes)]
  until valid_edge(u, v)
  return u, v end


-- Horizontal transfer
function hgt(gene, u, v)
   local sibling
   if u.parent[1] == u
   then sibling = u.parent[2]
   else sibling = u.parent[1] end
   local grand = u.parent.parent
   sibling.parent = grand
   if grand[1] == u.parent
   then grand[1] = sibling
   else grand[2] = sibling end
   if grand == gene then gene.root = gene[1] end
   if u.parent.index then
   gene.nodes[u.parent.index] = nil end
   
   local p = v.parent
   local new = {u, v; parent = p}
   u.parent = new
   v.parent = new
   if p[1] == v then p[1] = new
   else p[2] = new end
end


-- Iterating over combinations
function permutations(t)
   local p = gsl.permutation(#t)
   local r = {}
   local function iter()
      if not p then return nil end
      for i = 1, #t do r[i] = t[p:get(i)] end
      p = p:next()
      return r end
   return iter
end

local function flip(edge)
   local u, v = unpack(edge)
   return {v, u} end

local function co_flippings(t)
   local r = {}
   for i = 0, 2^#t-1 do
      local bits = i
      for j = 1, #t do
         bit, bits = (bits%2 == 1), math.floor(bits/2)
         if bit then r[j] = flip(t[j])
         else r[j] = t[j] end
      end
      coroutine.yield(r)
   end
end

function flippings(t)
   return coroutine.wrap(function () co_flippings(t) end)
end


-- misc.
function random_partition(rng, arr, ...)
   local spec = {}
   local result = {}
   for i, n in ipairs(arg) do
      result[i] = {}
      for j = 1, n do spec[#spec+1] = i end end
   spec = rng:shuffle(spec)
   for i, j in ipairs(spec) do
      result[j][#result[j] + 1] = arr[i] end
   return unpack(result) end

function pprint(tree, out)
  if tree.root then
     pprint(tree.root, out)
     out:write(';')
  elseif tree.name then out:write(tree.name)
  else
     out:write('(')
     pprint(tree[1], out)
     out:write(', ')
     pprint(tree[2], out)
     out:write(')')
  end end

function str(u)
   if u.name then return (u.index-1) .. '-' .. u.name
   else
      local left = u[1]
      while not left.name do left = left[1] end
      left = left.name
      local right = u[2]
      while not right.name do right = right[2] end
      right = right.name
      return (u.index-1) .. '-' .. 'LCA(' .. left .. ',' .. right .. ')'
   end
end

