Contents of directory
=====================

This directory contains the various scripts and source files used to generate the simulated datasets and summarize the results of simulation runs.

cyano, yule25, yule50, yule75, 100taxa.newick, yuleTree_100.newick, and yuleTree_200.newick are all source trees or collections of source trees used in various runs.

lua-gsl.c is a Lua5.1 module that allows the scripts to use pseudorandom number generators from Gnu GSL. It is required because Lua does not have a built-in PRNG.

restrict.lua and restrict2.lua are used to create datasets with incomplete gene trees.

How to generate and test a typical simulated dataset
====================================================

1. Create a directory with 50 newick files containing a single species tree each. Name the files tree1.newick, tree2.newick ... tree50.newick and name it, for example, yule25.
(you could create a different # of trees or give the directory a different name, as long as you keep the naming convention for the files within. mchoose.lua depends on this naming convention.)

2. Invoke mchoose.lua like this:
$ lua mchoose.lua yule25 3 [seeds] > hw3/noise1000/chosen.txt
Where:
yule25 is the name of the directory you kept your source trees in.
3 is the # of highways you want to be chosen for each species tree.
[seeds] is a list of 50 numbers that will be used to seed the PRNG. The number of seeds has to match exactly the number of trees in the input directory. We pass the seeds explicitly instead of using the time because we want perfect reproducibility. If you want to exactly replicate the previous datasets, the seeds that were used for them are
"{42, 1379, 1421, 2800, 4221, 7021, 11242, 18263, 29505, 47768, 77273, 125041, 202314, 327355, 529669, 857024, 385893, 242117, 628010, 870127, 497337, 366664, 864001, 229865, 93066, 322931, 415997, 738928, 154125, 893053, 46378, 939431, 985809, 924440, 909449, 833089, 741738, 574027, 314965, 888992, 203157, 91349, 294506, 385855, 680361, 65416, 745777, 811193, 556170, 366563}"
mchoose writes it's output to stdout, so we redirect it to be saved in a file, in this case hw3/noise1000/chosen.txt (just a naming convention to let us know later that this is a dataset of trees with 3 implanted highways and 1000 random transfers.)

3. Now, for each input tree, create a set of gene trees like this:
$ lua edges.lua hw3/noise1000/chosen.txt 1 100 50 | xargs -IEDGES lua generate.lua yule25/tree1.newick "{seed=42, ngenes=1000, noise=1000}" EDGES > hw3/noise1000/set1.newick
I'll explain first what each part of this long command-line does, and then how they all work together.
generate.lua is the main dataset generation script. It takes as input a species tree and some parameters and outputs that same species tree and a set of gene trees based on it. The parameters are a seed for the PRNG (again, for reproducibility), the number of gene trees to generate, the number of random HGTs (noise) to add, and a list of highways to implant. If we were to invoke generate.lua directly we would've done it like this:
$ lua generate.lua yule25/tree1.newick "{seed=42, ngenes=1000, noise=1000}" "{{2, 16, width=100, split=50}, {80, 48, width=50, split=25}}" > foo/bar.newick
Here I give the list of highways directly on the command-line. There's a wide highway and a narrow highway, both with a 50% split (the numbers are different because splits are given in absolute # of genes per side, not percentages).
What we do here is that instead of giving the list of highways directly, we extract it from the file hw3/noise1000/chosen.txt by means of the script edges.lua . This very simple script, when invoked like this:
$ lua edges.lua hw3/noise1000/chosen.txt 1 100 50
It extracts the list of highways from the first line of the input file and formats it in a way suitable for generate.lua's consumption. The third and fourth parameters, width and split respectively, are supplied here on the command-line because they aren't specified anywhere in the input file. (Recall that the input file was produced by mchoose.lua, which only chooses a set of valid, non-conflicting highways and does not concern itself with widths and splits).
The final ingredient, xargs, is glue: when we say
$ foo | xargs -IEDGES [command line]
we mean "run [command line] with every occurence of the string 'EDGES' replaced by the output of 'foo' (the input to xargs)". So xargs lets us give generate.lua a list of highways extracted from chosen.txt (via edges.lua) instead of listing them directly.
(One last thing for this step: in the general case, the seed passed to generate.lua does not have to be the same as the seed passed to mchoose.lua . If you want to reproduce the old data sets exactly the list of seeds passed to generate.lua is the same as the list of seeds for mchoose.lua, but if you're generating new datasets you can use two different lists of seeds.)

4. After repeating step 3 50 times we have in hw3/noise1000/ files named set1.newick thru set50.newick - one complete simulated dataset. It is time to calculate edge scores and see if we can recover the implanted highways. Theoretically, we could do this using score.lua, which is HiDe's main executable. The reason we don't is that score.lua gives us the full list of edge scores, which could run into hundreds and thousands of lines per species tree. We obviously want to keep the whole list when running on real data, but for the simulated experiments we only need a small # (15) of the top-scoring edges for each run - anything more than that would only make us quickly run of hard-disk quota.

The script of choice for simulated experiments is mpgscore.lua . It's invoked like so:
$ lua edges.lua hw3/noise1000/chosen.txt 4 0 0 | xargs -IEDGES mpgscore.lua hw3/noise1000/set4.newick 2 3 EDGES > hw3/noise1000/set4.scores
This line means "run two scoring iterations on set4.newick, on each iteration add an actual highway to the list of ignored edges if one was among the three top scoring edges in the previous iteratio, or the top scoring edge otherwise." Since mpgscore needs to know which edges are the implanted highways, we tell it using the same trick w/ edges.lua and xargs (we arbitrarily give edges.lua 0 for width and split because mpgscore doesn't care about those two numbers). If mpgscore.lua doesn't need to know what the true highways are (because we're running a single iteration experiment or want to always ignore the previous top-scoring edge regardless of whether it is a highway) the invocation is simpler:
$ mpgscore.lua hw3/noise1000/set4.newick 2 0 > hw3/noise1000/set4.scores
means "run two iteration with no knowledge of the true highways".

Note that score calculation is the most computationally intensive step in this procedure, so if you have more than a few scoring runs to do you should run them in parallel using condor. According to our own measurements from Jan 12 2011, a single scoring iteration on rack-shamir1 costs around 740 cpu-seconds. All the other steps combined (generating a whole bunch of datasets with different parameters and summarizing the results from the scoring runs) take less than 15 cpu-minutes.

(Other scripts with "score" in their names that used to exist but are no longer needed are variscore.lua, used to compare the different variants of per-gene normalization (no longer needed because we settled on pg^2 as the best - it is the variant used by both score.lua and mpgscore.lua), and tscore.lua/tgscore.lua, used to time scoring runs using per-gene/global normalization, respectively. (all of the scripts are version-controlled and can be revived if needed, of course.))

5. After all the scoring runs are finished and each setN.newick has a corresponding setN.scores file, we can summarize the results with msummary.lua :
$ lua msummary.lua hw3/noise1000
... Actually, no, that wouldn't work. msummary was written with the output of score.lua/variscore.lua in mind, and will not give correct summaries given the output of mpgscore.lua .

Previously, when summarizing true multiple-iteration runs, the summary listed for each run just the # of implanted highways 'found'. This can be easily accomplished by counting the # of occurences of the phrase '(true highway)' in mpgscore's output, like in the following ad-hoc shellscript:
$ for I in `seq 50`; do echo "set $I :" `grep -c '(true highway)' hw3/noise1000/set$I.scores" >> summary-3-1000.txt; done;
