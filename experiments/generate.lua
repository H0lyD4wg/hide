#!/usr/bin/env lua

require 'io'
require 'gsl'
require 'genutils'

inhibit = true
require 'score'
inhibit = nil

local ifile = io.open(arg[1])
local species = ifile:read('*a') ifile:close()
species =  nwtree_parse(species)
species = make_species(species)

local defaults = {
seed=0,
ngenes=1000,
noise=1000
}

local args = loadstring('return ' .. (arg[2] or '{}'))()
setmetatable(args, {__index =
function (table, key)
   return rawget(table, key) or defaults[key]
end })

local hws = loadstring('return' .. (arg[3] or '{}'))()

local genes = {} for j = 1, args.ngenes do genes[j] = make_gene(species) end
local rng = gsl.rng('mt19937', args.seed)
for i, hw in ipairs(hws) do
   local hw_genes = rng:choose(genes, hw.width)
   local hw_genes_left, hw_genes_right =
      random_partition(rng, genes, hw.split, hw.width - hw.split)
   for i, gene in ipairs(hw_genes_left) do
      local u, v = gene.nodes[hw[1]], gene.nodes[hw[2]]
      hgt(gene, u, v) end
   for i, gene in ipairs(hw_genes_right) do
      local u, v = gene.nodes[hw[1]], gene.nodes[hw[2]]
      hgt(gene, v, u) end
end

local noise = rng:sample(genes, args.noise)
for i, gene in ipairs(noise) do
   local u, v = random_horizontal_edge(rng, enumerate_nodes(gene.root))
   hgt(gene, u, v) end

pprint(species, io.stdout) print '' print ''
for i, gene in ipairs(genes) do
   pprint(gene, io.stdout) print '' end

