# -*- mode: python -*-

top = '.'
out = 'build'

def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')
    ctx.load('gas')

def configure(ctx):
    ctx.env.INCLUDES = ['src', 'vendor/lua-5.1.5/src']
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')
    ctx.load('gas')

def build(ctx):
    ccflags = ['-O3', '-g', '-Wall']
    srcfiles = ctx.path.ant_glob(
        'src/*.cxx',
        excl = ['src/sanity.cxx', 'src/main.cxx'])
    srcfiles.extend(ctx.path.ant_glob('src/*.c'))
    fnames = [srcfile.srcpath() for srcfile in srcfiles]
    ctx.stlib(
        features = 'c cxx cxxstlib cstlib',
        source = fnames, target = 'hgt',
        cflags = ccflags, cxxflags = ccflags)
    ctx.add_manual_dependency(
        ctx.path.find_node('src/blob.S'),
        ctx.path.find_node('src/score.lua'))
    exe = [
        'src/main.cxx',
        'src/blob.S',
        'vendor/luafilesystem/src/lfs.o'
     ]
    ctx.read_stlib('lua', paths = ['vendor/lua-5.1.5/src'])
    ctx.read_stlib('lpeg', paths = ['vendor/lpeg-0.12.2'])
    ctx.program(
        features = 'cxx asm cxxprogram',
        target = 'hide', source = exe,
        lib = ['m', 'dl'],
        use = ['hgt', 'lpeg', 'lua'],
        cflags = ccflags, cxxflags = ccflags)
